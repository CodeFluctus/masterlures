package fi.masterlures;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.services.s3.AmazonS3Client;

import java.util.ArrayList;
import java.util.HashMap;

import fi.masterlures.AWSS3.Download;
import fi.masterlures.AWSS3.Util;
import fi.masterlures.Lures.FishingLure;
import fi.masterlures.Lures.UserDecisions;

public class BrowseLures extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, GoldenRetriever.RetrievingProgress, AdapterView.OnItemSelectedListener, MySQLCommunicator.MySQLCommunicatorInterface {

    private Spinner spinnerLColors, spinnerLSwimmingDepth, spinnerLManufacturer, spinnerLType;
    private ArrayList<GoldenRetriever> allGoldenRetrievers = new ArrayList<>();

    ////////////////////////////////////////////////////////////////////////////////////////////////
    private ListView listView;
    private FishingLureEngine fishingLureEngine;
    private FishingLure fishingLure;

    // The S3 client used for getting the list of objects in the bucket
    private AmazonS3Client s3;
    // An adapter to show the objects
    private SimpleAdapter simpleAdapter;
    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    private Util util;

    private final int READ_STORAGE_PERMISSION_REQUEST_CODE = 1234;
    private Download download = new Download();
    private String bucket = "";

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_lures);
        setTitle("MasterLures - Browse lures");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        checkPermissions();
        bucket = new AWSConfiguration(this).optJsonObject("S3TransferUtility").optString("Bucket");
        // initData();
        TransferNetworkLossHandler transferNetworkLossHandler;
        transferNetworkLossHandler = TransferNetworkLossHandler.getInstance(BrowseLures.this);



        findViewById(R.id.buttonSearch).setOnClickListener(this);

        //Olion konstruktorille annetaan parametrina resurssit, sekä resurssin id, josta halutaan tuottaa ArrayList.
        GoldenRetriever allLureColorsR = new GoldenRetriever(getResources(), R.array.lureColors);
        GoldenRetriever allLureSwimingDepths = new GoldenRetriever(getResources(), R.array.lureSwimingDepth);
        GoldenRetriever allLureManufacturers = new GoldenRetriever(getResources(), R.array.lureManufacturer);
        GoldenRetriever allLureTypes = new GoldenRetriever(getResources(), R.array.lureType);

        //Lisätään kaikki oliot ArrayListiin, jossa ne "käynnistetään"
        allGoldenRetrievers.add(allLureColorsR);
        allGoldenRetrievers.add(allLureSwimingDepths);
        allGoldenRetrievers.add(allLureManufacturers);
        allGoldenRetrievers.add(allLureTypes);

        for(int i = 0; i < allGoldenRetrievers.size(); i++)
        {
            allGoldenRetrievers.get(i).setCallback(this);
            allGoldenRetrievers.get(i).execute("");
        }

        //Sidotaan aikaisemmin määritetyt Spinner-oliot näkymään ja asetetaan niille OnItemSelecterListenerit.
        spinnerLType = findViewById(R.id.spinnerLType);
        spinnerLColors = findViewById(R.id.spinnerLColor);
        spinnerLSwimmingDepth = findViewById(R.id.spinnerLDepth);
        spinnerLManufacturer = findViewById(R.id.spinnerLManufacturer);

        spinnerLType.setOnItemSelectedListener(this);
        spinnerLColors.setOnItemSelectedListener(this);
        spinnerLSwimmingDepth.setOnItemSelectedListener(this);
        spinnerLManufacturer.setOnItemSelectedListener(this);

        Log.d("ASD", "OnCreate ready");
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.buttonSearch)
        {
            //Tallennetaan käyttäjän valinnat olioon.
            UserDecisions userDecisions = FishingLureApplication.giveEngineToUserDecisions();
            userDecisions.setDlManufacturer(spinnerLManufacturer.getSelectedItem().toString());
            userDecisions.setDlColor(spinnerLColors.getSelectedItem().toString());
            userDecisions.setDlSwimmingDepth(spinnerLSwimmingDepth.getSelectedItem().toString());
            userDecisions.setDlType(spinnerLType.getSelectedItem().toString());


            Log.d("ASD", "Search button pushed!");
            String wantedUrl= "https://koodiaalto.fi/masterluresPHP/"; //customQueryToLures.php
            MySQLCommunicator mySQLCommunicator = new MySQLCommunicator(wantedUrl);
            mySQLCommunicator.setPHPscript("customQueryToLures.php");
            mySQLCommunicator.setCallback(this);
            mySQLCommunicator.execute();


        }
    }


    //Suoritetaan kun GoldenRetriever on suorittanut doInBackground-metodinsa ja kutsuu onPostExecute-metodiaan
    @Override
    public void retrievingDone(ArrayList dropListItems, int resourceId) {

        //Määritellään adapteri DropDownViewille ja asetetaan sille näkymäresurssi
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dropListItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Päätetään oliolta saatavan parametrin mukaan, että mille spinnerille asetetaan näkymään
        //siltä saatu ArrayList.
        if(resourceId == R.array.lureColors)
        {
            spinnerLColors.setAdapter(adapter);
        }
        else if(resourceId == R.array.lureSwimingDepth)
        {
            spinnerLSwimmingDepth.setAdapter(adapter);
        }
        else if(resourceId == R.array.lureManufacturer)
        {
            spinnerLManufacturer.setAdapter(adapter);
        }
        else if(resourceId == R.array.lureType)
        {
            spinnerLType.setAdapter(adapter);
        }
    }

    @Override
    public void onUpdate(Integer percentageDone) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ASD", "Itemi valittu:" + i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void searchingDone() {

        fishingLureEngine = FishingLureApplication.giveEngine();
        for(int i = 0; i < fishingLureEngine.numberOfLures(); i++)
        {
            fishingLure = fishingLureEngine.giveLure(i);
            Log.d("ASD","Manuf: " + fishingLure.getManufacturer());
            Log.d("ASD","Type: " + fishingLure.getType());
            Log.d("ASD","Name: " + fishingLure.getName());
            Log.d("ASD","Model: " + fishingLure.getModel());
            Log.d("ASD","Color: " + fishingLure.getColor());
            Log.d("ASD","Depth: " + fishingLure.getSwimmingDepth());
            Log.d("ASD","imageUrl: " + fishingLure.getImageUrl() + "\n");
        }

        //Ladataan kuvat AWS S3 bucketista.
        download.startDownload(BrowseLures.this);

        Runnable r = new Runnable() {
            @Override
            public void run(){
                //Asetetaan vieheet ListViewiin näkyville.
                listView = findViewById(R.id.listViewBrowseLures);
                CustomAdapter customAdapter = new CustomAdapter();

                listView.setAdapter(customAdapter);
                listView.setOnItemClickListener(BrowseLures.this);
            }
        };

        Handler h = new Handler();
        h.postDelayed(r, 1000); // <-- the "1000" is the delay time in miliseconds.

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d("DDD", "onItemClick: Items position = " + position);

        Log.d("OmaPakkiActivity", "onClick: buttonAddLure!");
        Intent intent = new Intent(this, AddLureToOwnKitActivity.class);
        intent.putExtra("position", position);
        startActivity(intent);

    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////// OMIA SÖKERRYKSIÄ ////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////


    public Drawable getDrawableFromExternalStorage(int position) {
        fishingLure = fishingLureEngine.giveLure(position);
        String path = "/storage/emulated/0/Android/data/fi.masterlures/files/lure_images/" + fishingLure.getImageUrl();
        Drawable drawable = Drawable.createFromPath(path);

        /*Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        //Skaalataan kuvaa kokoon 1300px x 900px.
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 1300, 900, true));
        //Palautetaan skaalattu drawable "d".
        return d;*/

        return drawable;
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return fishingLureEngine.numberOfLures();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.custom_layout_browse_lures, parent, false);
            }

            ImageView imageView = convertView.findViewById(R.id.imageViewBrowseLures);
            TextView textViewManufacturer = convertView.findViewById(R.id.textViewManufacturerBrowseLures);
            TextView textViewType = convertView.findViewById(R.id.textViewModelBrowseLures);

            fishingLure = fishingLureEngine.giveLure(position);

            Drawable drawable = getDrawableFromExternalStorage(position);
            if(drawable != null) {
                imageView.setImageDrawable(drawable);
            }

            textViewManufacturer.setText(fishingLure.getManufacturer());
            textViewType.setText(fishingLure.getModel());

            return convertView;
        }
    }

    private void initData() {
        // Gets the default S3 client.
        s3 = util.getS3Client(BrowseLures.this);
        //transferRecordMaps = new ArrayList<HashMap<String, Object>>();
    }

    public void checkPermissions() {

        //READ_STORAGE_PERMISSION_REQUEST_CODE = ContextCompat.checkSelfPermission(BrowseLures.this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (READ_STORAGE_PERMISSION_REQUEST_CODE != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(BrowseLures.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(BrowseLures.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_STORAGE_PERMISSION_REQUEST_CODE);
                Log.d("checkPermissions", "checkPermissions: READ_EXTERNAL_STORAGE = " + READ_STORAGE_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    download.startDownload(BrowseLures.this);
                } else {
                    // Permission Denied
                    Toast.makeText(BrowseLures.this, "READ_EXTERNAL_STORAGE Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        navigateUpTo();

        /*Context context = getApplicationContext();
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/

        return true;
    }

    private void navigateUpTo() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////// OMIA SÖKERRYKSIÄ ////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////


}
