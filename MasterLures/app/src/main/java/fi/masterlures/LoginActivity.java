package fi.masterlures;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fi.masterlures.Lures.PasswordHasher;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, MySQLCommunicator.MySQLCommunicatorInterface {

    private Button loginButton, createAccountButton;
    private EditText etUsername, etPassword;
    private MasterLuresUser masterLuresUser = null;
    private String userInputPassword, userInputUsername;
    private int backButtonCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("MasterLures");



        loginButton = findViewById(R.id.loginBtn);
        loginButton.setOnClickListener(this);

        createAccountButton = findViewById(R.id.createAccountBtn);
        createAccountButton.setOnClickListener(this);
        //findViewById(R.id.loginBtn).setOnClickListener(this);
        //findViewById(R.id.createAccountBtn).setOnClickListener(this);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.loginBtn)
        {
            masterLuresUser = FishingLureApplication.giveMasterLuresUser();
            masterLuresUser.setPasswordFromDB(null);
            masterLuresUser.setSaltFromDB(null);


            Log.d("LoginActivity", "onClick: loginBtn!");
            userInputPassword = etPassword.getText().toString();
            userInputUsername = etUsername.getText().toString();
            masterLuresUser.setUsername(userInputUsername);


            MySQLCommunicator mySQLCommunicator = new MySQLCommunicator("https://koodiaalto.fi/masterluresPHP/");
            mySQLCommunicator.setPHPscript("loginUser.php");
            mySQLCommunicator.setCallback(this);
            mySQLCommunicator.execute();




            //Intent intent = new Intent(this, MainActivity.class);
            //startActivity(intent);
        }
        else if(view.getId() == R.id.createAccountBtn)
        {
            Log.d("LoginActivity", "onClick: createAccountBtn!");
            Intent intent = new Intent(this, CreateAccountActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void searchingDone() {
        Log.d("ASD", "Mainissa Ollaan!");
        if(masterLuresUser.getSaltFromDB() == null || masterLuresUser.getPasswordFromDB() == null)
        {
            Toast.makeText(this,"Username " + masterLuresUser.getUsername() + " not found!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Log.d("ASD","Password:" + userInputPassword);
            PasswordHasher passwordHasher = new PasswordHasher();
            if(passwordHasher.comparePasswords(userInputPassword, masterLuresUser.getPasswordFromDB(),masterLuresUser.getSaltFromDB()))
            {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(this,"Wellcome " + masterLuresUser.getUsername(), Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this,"Invalid password!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(backButtonCount >= 1){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }else {
            Toast.makeText(this, "Press the backbutton once again to close the application", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }
}
