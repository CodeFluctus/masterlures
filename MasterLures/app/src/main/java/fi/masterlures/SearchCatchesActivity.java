package fi.masterlures;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;

import fi.masterlures.Lures.FishingLure;

public class SearchCatchesActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SearchCatchesActivity";

    private RadioGroup rgSearch;
    private RadioButton rbUser;
    private RadioButton rbFish;
    private RadioButton rbLure;
    private RadioButton rbFishingMethod;
    private RadioButton rbWaterType;
    private Button buttongetSearchData;
    private Button buttonSearchByLure;
    private Spinner spinnerSearch;
    private TextView tvLureUsed;
    private ListView lvSearchResult;
    private String[] searchSpinnerValues;
    private String answer;
    private ArrayList<HashMap<String, String>> arrayList;
    private int lureId = -1;
    private FishingLure fishingLure;
    private FishingLureEngine fishingLureEngine;

    @Override
    protected void onCreate(Bundle savedInstanceStateSearch) {
        super.onCreate(savedInstanceStateSearch);
        setContentView(R.layout.activity_search_catches);
        setTitle("MasterLures - Browse catches");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        rgSearch = findViewById(R.id.radioSearchGroup);
        rbUser = findViewById(R.id.rbSearchByUser);
        rbFish = findViewById(R.id.rbSearchByFish);
        rbLure = findViewById(R.id.rbSearchByLure);
        rbFishingMethod = findViewById(R.id.rbSearchByfishingMethod);
        rbWaterType = findViewById(R.id.rbSearchByWaterType);

        buttongetSearchData = findViewById(R.id.buttonGetCatch);
        buttonSearchByLure = findViewById(R.id.buttonSearchByLure);
        buttongetSearchData.setOnClickListener(this);
        buttonSearchByLure.setOnClickListener(this);

        tvLureUsed = findViewById(R.id.tvLureUsed);

        spinnerSearch = findViewById(R.id.spinnerSearch);

        lvSearchResult = findViewById(R.id.searchResultListView);

        arrayList = new ArrayList<>();
        // Jos on haettu viehe niin palatessa tallennetaan vieheen ID muuttujaan
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            lureId = extras.getInt("position");
            Log.d(TAG, "onCreate: LureID " + lureId);
        }
        //Jos viehe on haettu niin palatessa takaisin chekataan vieheen haun täppä ja laitetaan vieheen hakuun
        // liittyvät kentät aktiiviseksi
        if (lureId != -1) {
            rgSearch.check(R.id.rbSearchByLure);
            buttonSearchByLure.setVisibility(View.VISIBLE);
            tvLureUsed.setVisibility(View.VISIBLE);
            spinnerSearch.setVisibility(View.GONE);
            // Haetaan vieheen ID:n avulla nimi vieheelle ja asetaan se tekstikenttään
            fishingLureEngine = FishingLureApplication.giveEngine();
            fishingLure = fishingLureEngine.giveLure(lureId);
            tvLureUsed.setText(fishingLure.getModel());
        }
        // Kuunnellaan minkä valinnan käyttäjä tekee ja aktivoidaan sen mukaan tarvittavat kentät ja arvot
        rgSearch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(rbUser.isChecked()){
                    spinnerSearch.setVisibility(View.VISIBLE);
                    buttonSearchByLure.setVisibility(View.GONE);
                    tvLureUsed.setVisibility(View.GONE);
                    searchSpinnerValues = getResources().getStringArray(R.array.user);
                    spinnerkokeilu(spinnerSearch, searchSpinnerValues);
                }
                else if(rbFish.isChecked()){
                    spinnerSearch.setVisibility(View.VISIBLE);
                    buttonSearchByLure.setVisibility(View.GONE);
                    tvLureUsed.setVisibility(View.GONE);
                    searchSpinnerValues = getResources().getStringArray(R.array.fishTypeSearch);
                    spinnerkokeilu(spinnerSearch, searchSpinnerValues);
                }
                else if(rbLure.isChecked()){
                    buttonSearchByLure.setVisibility(View.VISIBLE);
                    tvLureUsed.setVisibility(View.VISIBLE);
                    spinnerSearch.setVisibility(View.GONE);
                }
                else if(rbFishingMethod.isChecked()){
                    spinnerSearch.setVisibility(View.VISIBLE);
                    buttonSearchByLure.setVisibility(View.GONE);
                    tvLureUsed.setVisibility(View.GONE);
                    searchSpinnerValues = getResources().getStringArray(R.array.catchMethodSearch);
                    spinnerkokeilu(spinnerSearch, searchSpinnerValues);
                }
                else if(rbWaterType.isChecked()){
                    spinnerSearch.setVisibility(View.VISIBLE);
                    buttonSearchByLure.setVisibility(View.GONE);
                    tvLureUsed.setVisibility(View.GONE);
                    searchSpinnerValues = getResources().getStringArray(R.array.waterTypeSearch);
                    spinnerkokeilu(spinnerSearch, searchSpinnerValues);
                }
            }
        });
    }
    //Metodi, jolla spinnerit alustetaan ja määrätään niille arvot
    private void spinnerkokeilu(Spinner spinner, String[] list){
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setPrompt("Valitse:");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            // Painettaessa hakunappia käynnistyy tietojen haku tietokannasta
            case (R.id.buttonGetCatch):
                Log.d(TAG, "onClick: buttonSearch!");
                //Aluksi tyhjennetään lista aiemmista hauista
                arrayList.clear();
                //Suoritetaan tietojen haku omassa threadissa
                Thread thread= new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //Määritetään skripti mitä käytetään
                            String catchPhpUrl = "https://koodiaalto.fi/masterluresPHP/searchCatch.php";
                            URL catchUrl = new URL(catchPhpUrl);
                            Log.d(TAG, "run: URL: " + catchUrl);

                            JSONObject postDataParams = new JSONObject();
                            //Lähetettävä hakudata riippuu siitä minkä valinnan käyttäjä on aiemmin valinnut
                            switch(rgSearch.getCheckedRadioButtonId()) {
                                case (R.id.rbSearchByUser):
                                    postDataParams.put("userId", spinnerSearch.getSelectedItem().toString());
                                    break;
                                case (R.id.rbSearchByLure):
                                    postDataParams.put("lureUsed", String.valueOf(lureId));
                                    break;
                                case (R.id.rbSearchByfishingMethod):
                                    postDataParams.put("catchMethod", spinnerSearch.getSelectedItem().toString());
                                    break;
                                case (R.id.rbSearchByWaterType):
                                    postDataParams.put("waterType", spinnerSearch.getSelectedItem().toString());
                                    break;
                                case (R.id.rbSearchByFish):
                                    postDataParams.put("fishType", spinnerSearch.getSelectedItem().toString());
                                    break;
                            }
                            Log.d(TAG, "run: JSON: " + postDataParams.toString());
                            //Avataan yhteys url:iin
                            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) catchUrl.openConnection();
                            httpsURLConnection.setRequestMethod("GET");
                            //Määritelään, että syötetään verkkosivulle dataa.
                            httpsURLConnection.setDoOutput(true);
                            //Noudetaan Outputstream, johon voidaan kirjoittaa BufferedWriterilla dataa.
                            OutputStream outputStream = httpsURLConnection.getOutputStream();
                            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                            //Muutetaan metodilla aikaisemmin luotu JSON-olio string muotoon ja kirjoitetaan se sivulle.
                            bufferedWriter.write(convertJSONtoString(postDataParams));
                            //Suljetaan bufferedWriter, jolloin se myös tyhjentää ennen sulkua kirjoittimen.
                            bufferedWriter.close();
                            //Suljetaan Outputstream.
                            outputStream.close();

                            //Noudetaan Inputstream
                            InputStream stream = httpsURLConnection.getInputStream();

                            //Muutetaan Inputstream string muotoon metodilla.
                            answer = convertInputStreamToString(stream);
                            Log.d("ASD", answer);
                            //Siivotaan vastauksesta alun ylimääräiset merkit {-merkkiin asti
                            // Näin saadaan JSON:in mukainen tekstijono, joka voidaan muuttaa JSONobjektiksi
                            int position = answer.indexOf("{");
                            String newAnswer = answer.substring(position);

                            Log.d(TAG, "runNewAnswer: " + newAnswer);
                                //Luodaan vastauksesta JSONobjekti
                                JSONObject answerObject = new JSONObject(newAnswer);
                                // luodaan array joka sisältää kaljoen tiedot
                                JSONArray jsonAnswer = answerObject.getJSONArray("fish");
                                //luupataan array läpi ja haetaan sieltä kalojen nimet ja määrät
                                for (int i = 0; i < jsonAnswer.length(); i++) {
                                    //tallennetaan uuteen JsonObjectiin yhden kalan tiedot kerrallaan
                                    JSONObject tt = jsonAnswer.getJSONObject(i);
                                    //tallennetaan kalan laji ja määrä omiin muuttujiin
                                    String fishType = tt.getString("fishType");
                                    String count = tt.getString("count");
                                    Log.d("JPTAG", fishType);
                                    Log.d("JPTAG", count);

                                    //Syötetään kalan laji ja määrä uuteen Hashmappiinn
                                    HashMap<String, String> fishList = new HashMap<>();
                                    fishList.put("fishType", fishType);
                                    fishList.put("count", count);

                                    // Syötetään Hashmapin tiedot arrayhin
                                    arrayList.add(fishList);
                                    Log.d(TAG, "run arrayList: "+ arrayList.toString());
                                }
                                //Kutsutaan metodia, jolle annetaan arraylist muuttujaksi
                                showResult(arrayList);

                        }catch (IOException | JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
                //Käynnistetään tietojen haku omassa threadissa
                thread.start();
                break;
            //Käynnistetään BrowseLures-aktiviteetti jos haetaan viehettä
            case (R.id.buttonSearchByLure):
                Intent intent = new Intent(this, BrowseLures.class);
                startActivity(intent);
                break;
        }
    }
    //Metodi, jolla luodaan lista tietokannasta saadusta vastauksesta kalojen tyypeistä ja lukumääristä
    private void showResult(final ArrayList<HashMap<String, String>> arrayList){
        //Suoritetaan metodi UI-threadissa
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListAdapter adapter = new SimpleAdapter(SearchCatchesActivity.this, arrayList, R.layout.list_item, new String[]{"fishType", "count"}, new int[]{R.id.tvFishType, R.id.tvCount});
                lvSearchResult.setAdapter(adapter);
            }
        });
    }
    //Metodi, jolla JSONin muutetaan Stringiksi
    private String convertJSONtoString(JSONObject jsonObject)
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = jsonObject.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (first)
            {
                first = false;
            }
            else
            {
                result.append("&");
            }
            try {
                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }
    //Metodi, jolla InputStreamin data muutetetaan Stringiksi
    private String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
             * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
             * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
             * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Palautetaan luotu stringi.
        return stringToReturn;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return true;
    }
}
