package fi.masterlures;

public class MasterLuresUser {

    private String saltFromDB, passwordFromDB, username, dbError = null;

    public String getSaltFromDB() {
        return saltFromDB;
    }

    public void setSaltFromDB(String saltFromDB) {
        this.saltFromDB = saltFromDB;
    }

    public String getPasswordFromDB() {
        return passwordFromDB;
    }

    public void setPasswordFromDB(String passwordFromDB) {
        this.passwordFromDB = passwordFromDB;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDbError() {
        return dbError;
    }

    public void setDbError(String dbError) {
        this.dbError = dbError;
    }
}
