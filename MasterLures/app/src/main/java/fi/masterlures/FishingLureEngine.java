package fi.masterlures;

import java.util.ArrayList;

import fi.masterlures.Lures.FishingLure;

public class FishingLureEngine {
    ArrayList<FishingLure> allLures = new ArrayList<>();

    public void addLure(FishingLure fishingLure)
    {
        allLures.add(fishingLure);
    }

    public void clearAllLures()
    {
        allLures.clear();
    }

    public void deleteLure(FishingLure fishingLure) {
        allLures.remove(fishingLure);
    }

    public int numberOfLures()
    {
        return allLures.size();
    }

    public FishingLure giveLure(int index)
    {
        if(allLures.size() > index)
        {
            return allLures.get(index);
        }
        else
        {
            return null;
        }
    }
}
