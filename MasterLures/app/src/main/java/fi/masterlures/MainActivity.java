package fi.masterlures;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import fi.masterlures.Lures.PasswordHasher;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonOwnKit,buttonAddCatch,buttonLogout, buttonBrowseLures, buttonBrowseCatches;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button button_asetukset = findViewById(R.id.button_asetukset);
        buttonOwnKit = findViewById(R.id.buttonOwnKit);
        buttonAddCatch = findViewById(R.id.buttonAddCatch);
        buttonLogout = findViewById(R.id.buttonLogout);
        buttonBrowseLures = findViewById(R.id.buttonBrowseLures);
        buttonBrowseCatches = findViewById(R.id.buttonBrowseCatches);

        buttonAddCatch.setOnClickListener(this);
        buttonOwnKit.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        buttonBrowseLures.setOnClickListener(this);
        buttonBrowseCatches.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        /*Log.d("MainActivity", "onClick: omaPakki!");
        Intent intent = new Intent(this, OmaPakkiActivity.class);
        startActivity(intent);*/

        /*Button buttonAddCatch = findViewById(R.id.buttonAddCatch);
        Button buttonOwnKit = findViewById(R.id.buttonOwnKit);
        Button buttonLogout = findViewById(R.id.buttonLogout);*/

        if (view == buttonOwnKit) {
            Log.d("MainActivity", "onClick: omaPakki!");
            Intent intent = new Intent(this, OmaPakkiActivity.class);
            startActivity(intent);
        } else if (view == buttonAddCatch) {
            Log.d("MainActivity", "onClick: syotaSaalis!");
            Intent intent = new Intent(this, CatchActivity.class);
            startActivity(intent);
        } else if(view == buttonLogout) {
            Log.d("MainActivity", "onClick: kirjauduUlos!");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.buttonBrowseLures)
        {
            Log.d("MainActivity", "onClick: selaaVieheitä!");
            Intent intent = new Intent(this, BrowseLures.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.buttonBrowseCatches)
        {
            Log.d("MainActivity", "onClick: EtsiSaaliita!");
            Intent intent = new Intent(this, SearchCatchesActivity.class);
            startActivity(intent);
        }
    }
}
