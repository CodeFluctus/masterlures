package fi.masterlures;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import fi.masterlures.Lures.FishingLure;

public class AddLureToOwnKitActivity extends AppCompatActivity implements View.OnClickListener {

    FishingLure fishingLure;
    FishingLureEngine fishingLureEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("MasterLures - Add Lure");
        setContentView(R.layout.activity_add_lure_to_own_kit);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        int positionToLure = 0;
        positionToLure = intent.getIntExtra("position", positionToLure);
        Log.d("DDD", "onCreate: positionToLure = " + positionToLure);

        fishingLureEngine = FishingLureApplication.giveEngine();
        fishingLure = fishingLureEngine.giveLure(positionToLure);

        ImageView imageView = findViewById(R.id.imageViewAddLure);
        TextView textViewAddLureManufacturer = findViewById(R.id.textViewAddLureManufacturer);
        TextView textViewAddLureType = findViewById(R.id.textViewAddLureType);
        TextView textViewAddLureModel = findViewById(R.id.textViewAddLureModel);
        TextView textViewAddLureColor = findViewById(R.id.textViewAddLureColor);
        TextView textViewAddLureSwimDepth = findViewById(R.id.textViewAddLureSwimDepth);

        Drawable drawable = getDrawableFromExternalStorage(positionToLure);

        if(drawable != null) {
            imageView.setImageDrawable(drawable);
        }

        textViewAddLureManufacturer.setText(fishingLure.getManufacturer());
        textViewAddLureType.setText(fishingLure.getType());
        textViewAddLureModel.setText(fishingLure.getModel());
        textViewAddLureColor.setText(fishingLure.getColor());
        textViewAddLureSwimDepth.setText(fishingLure.getSwimmingDepth());

        Button buttonAddLureAddToOmaPakki = findViewById(R.id.buttonAddLureAddToOwnKit);
        buttonAddLureAddToOmaPakki.setOnClickListener(this);

        Button buttonAddLureToSearch = findViewById(R.id.buttonAddLureToSearch);
        buttonAddLureToSearch.setOnClickListener(this);

        Button buttonAddLureToCatch = findViewById(R.id.buttonAddLureToCatch);
        buttonAddLureToCatch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d("DDD", "onClick: Lisää OmaPakkiin painettu!");

        switch (v.getId()) {
            case (R.id.buttonAddLureAddToOwnKit):
                Intent intent = getIntent();
                int position = 0;
                position = intent.getIntExtra("position", position);
                Log.d("DDD", "onClick: position = " + position);

                Intent intent2 = new Intent(this, OmaPakkiActivity.class);
                intent2.putExtra("position", position);
                startActivity(intent2);
                break;
            case (R.id.buttonAddLureToSearch):
                Intent intentSearch = getIntent();
                int positionSearch = 0;
                positionSearch = intentSearch.getIntExtra("position", positionSearch);
                Log.d("DDD", "onClick: position = " + positionSearch);

                Intent intentSearch2 = new Intent(this, SearchCatchesActivity.class);
                intentSearch2.putExtra("position", positionSearch);
                startActivity(intentSearch2);
                break;
            case (R.id.buttonAddLureToCatch):
                Intent intentCatch = getIntent();
                int positionCatch = 0;
                positionCatch = intentCatch.getIntExtra("position", positionCatch);
                Log.d("DDD", "onClick: position = " + positionCatch);

                Intent intentCatch2 = new Intent(this, CatchActivity.class);
                intentCatch2.putExtra("position", positionCatch);
                startActivity(intentCatch2);
                break;
        }
    }

    public Drawable getDrawableFromExternalStorage(int position) {
        fishingLure = fishingLureEngine.giveLure(position);
        String path = "/storage/emulated/0/Android/data/fi.masterlures/files/lure_images/" + fishingLure.getImageUrl();
        Drawable drawable = Drawable.createFromPath(path);

        /*Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        //Skaalataan kuvaa kokoon 1300px x 900px.
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 1300, 900, true));
        //Palautetaan skaalattu drawable "d".
        return d;*/
        return drawable;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        /*Context context = getApplicationContext();
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/

        return true;
    }
}
