package fi.masterlures;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
//Interface, jolla saadaan koordinaattitieto lähetettyä kutsuvalle aktiviteetille
interface CoordinatesInterface{

    void onMessage(String lat, String lon);
    void onStatusChange(String newStatus);
}
//Luokka, jonka metodeilla saadaan haettua puhelimen paikkatieto
class GetCoordinates {
    //Interfacen kuuntelija, jonka avulla kuunnellaan milloin metodilla on lähetettävää tietoa
    private final CoordinatesInterface observer;
    FusedLocationProviderClient fusedLocationClient;

    String latitudeMin = null;
    String longitudeMin = null;
    Double latitude = 0.0;
    Double longitude = 0.0;

    public GetCoordinates(CoordinatesInterface observer) {
        this.observer = observer;
    }
    //Metodi, jolla koordinaatit haetaan
    public void getLocation(Context context) {
        Log.d("GPSdata1", "toimiiko", null);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                observer.onStatusChange("location");

                if (location != null){
                    //Syötetään koordinaatit omille muuttujille
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    //Muutetaan koordinaattien muoto haluttuun formaattiin
                    latitudeMin = location.convert(location.getLatitude(), Location.FORMAT_DEGREES);
                    longitudeMin = location.convert(location.getLongitude(), Location.FORMAT_DEGREES);
                    Log.d("GPSSuccess","onGPSSuccess: " + latitudeMin + " "+ longitudeMin);
                }
                //lähetetään observerin avulla koordinaatit eteenpäin
                observer.onMessage(latitudeMin, longitudeMin);
            }
        });
    }
}
