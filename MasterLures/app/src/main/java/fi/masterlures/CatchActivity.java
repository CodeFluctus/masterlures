package fi.masterlures;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

import fi.masterlures.Lures.FishingLure;

public class CatchActivity extends AppCompatActivity implements View.OnClickListener, CoordinatesInterface {

    private static final String TAG = "CatchActivity";

    private final String[] appPermissions ={Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET };
    private static final int MY_PERMISSIONS_REQUEST_CODE= 908;

    private EditText etDateTime;
    private EditText etLatitude;
    private EditText etLongitude;
    private EditText etFishWeigth;
    private EditText etLureUsed;
    private EditText etWaterTemp;
    private EditText etWeatherTemp;
    private EditText etWindStrength;
    private Spinner spinnerFish;
    private Spinner spinnerFishingStyle;
    private Spinner spinnerWaterType;
    private Spinner spinnerWaterMurkness;
    private Spinner spinnerWeather;
    private Spinner spinnerVisibility;
    private Spinner spinnerCatchDepth;
    private Spinner spinnerSalinity;
    private Button buttonGetLure;
    private Button buttonGetCooordinates;
    private Button buttonSave;
    private RadioGroup rgWeight;
    private RadioButton rbWeightEstimated;
    private RadioButton rbWeightMeasured;
    private int lureId = -1;
    private FishingLure fishingLure;
    private FishingLureEngine fishingLureEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catch);
        setTitle("MasterLures - Add new catch");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Pvm ja ajan automaattinen täyttö nykyiseen ajankohtaan
        etDateTime = findViewById(R.id.etDateTime);
        etDateTime.setText(dateTime());
        etLatitude = findViewById(R.id.etLatitude);
        etLongitude = findViewById(R.id.etLongitude);
        etFishWeigth = findViewById(R.id.etWeight);
        etLureUsed = findViewById(R.id.etLureUsed);
        etWaterTemp = findViewById(R.id.etWaterTemp);
        etWeatherTemp = findViewById(R.id.etTemperature);
        etWindStrength = findViewById(R.id.etWindStrength);

        rgWeight = findViewById(R.id.radioWeightGroup);
        rbWeightEstimated = findViewById(R.id.rbWeightEstimated);
        rbWeightMeasured = findViewById(R.id.rbWeightMeasured);

        buttonGetLure = findViewById(R.id.buttonGetLure);
        buttonGetLure.setOnClickListener(this);
        buttonGetCooordinates = findViewById(R.id.buttonCoordinates);
        buttonGetCooordinates.setOnClickListener(this);
        buttonSave = findViewById(R.id.buttonSaveCatch);
        buttonSave.setOnClickListener(this);

        // Spinnereille resurssit
        spinnerFish = findViewById(R.id.spinnerFishType);
        spinnerFishingStyle = findViewById(R.id.spinnerFishingStyle);
        spinnerWaterType = findViewById(R.id.spinnerWaterType);
        spinnerWaterMurkness = findViewById(R.id.spinnerWaterMurkness);
        spinnerWeather = findViewById(R.id.spinnerWeather);
        spinnerVisibility = findViewById(R.id.spinnerVisibility);
        spinnerCatchDepth = findViewById(R.id.spinnerCatchDepth);
        spinnerSalinity = findViewById(R.id.spinnerWaterSalinity);

        //Spinnereille arvot
        String[] fishtype = getResources().getStringArray(R.array.fishType);
        spinnerkokeilu(spinnerFish, fishtype);

        String[] fishingstyle = getResources().getStringArray(R.array.catchMethod);
        spinnerkokeilu(spinnerFishingStyle, fishingstyle);

        String[] waterType = getResources().getStringArray(R.array.waterType);
        spinnerkokeilu(spinnerWaterType, waterType);

        String[] waterMurkness = getResources().getStringArray(R.array.waterMurkness);
        spinnerkokeilu(spinnerWaterMurkness, waterMurkness);

        String[] weatherVisibility = getResources().getStringArray(R.array.weatherVisibility);
        spinnerkokeilu(spinnerVisibility, weatherVisibility);

        String[] weatherConditions = getResources().getStringArray(R.array.weatherConditions);
        spinnerkokeilu(spinnerWeather, weatherConditions);

        String[] catchDepth = getResources().getStringArray(R.array.catchDepth);
        spinnerkokeilu(spinnerCatchDepth, catchDepth);

        String[] waterSalinity = getResources().getStringArray(R.array.waterSalinity);
        spinnerkokeilu(spinnerSalinity, waterSalinity);

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            lureId = extras.getInt("position");
            Log.d(TAG, "onCreate: LureID " + lureId);

        }
        if(lureId != -1) {
            fishingLureEngine = FishingLureApplication.giveEngine();
            fishingLure = fishingLureEngine.giveLure(lureId);
            etLureUsed.setText(fishingLure.getModel());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.buttonGetLure):
                //käynnistetään BrowseLures-aktiviteetti ja haetaan käytetty viehe
                Log.d(TAG, "onClick: buttonGetLure!");
                Intent intent = new Intent(this, BrowseLures.class);
                startActivity(intent);
                break;

            case (R.id.buttonCoordinates):
                //Pyydetän lupa käyttää sijaintia ja tallennetaan koordinaatit EditTexteihin
                Log.d(TAG, "onClick: button");
                if (checkAndRequestPermissions()){
                    try {
                        GetCoordinates getCoordinates = new GetCoordinates(CatchActivity.this);
                        getCoordinates.getLocation(this);
                    } catch (Exception e){
                        Log.d("TAG", e.toString());
                    }
                }
                break;
            case (R.id.buttonSaveCatch):
                Log.d(TAG, "onClick: buttonSaveCatch!");
                //Muuttujat, joihin tallennetaan käyttäjän valinnat
                String catchDateTime = etDateTime.getText().toString();
                String fish = spinnerFish.getSelectedItem().toString();
                String weight = etFishWeigth.getText().toString();
                String weightMode;
                if (rbWeightEstimated.isChecked()){
                    weightMode = "Estimated";
                }else {
                    weightMode = "Measured";
                }
                String lureUsed = String.valueOf(lureId);
                String fishingStyle = spinnerFishingStyle.getSelectedItem().toString();
                String waterType = spinnerWaterType.getSelectedItem().toString();
                String waterTemp = etWaterTemp.getText().toString();
                String catchDepth = spinnerCatchDepth.getSelectedItem().toString();
                String waterMurkness = spinnerWaterMurkness.getSelectedItem().toString();
                String latitude = etLatitude.getText().toString();
                String longitude = etLongitude.getText().toString();
                String weather = spinnerWeather.getSelectedItem().toString();
                String weatherTemp = etWeatherTemp.getText().toString();
                String windStrength = etWindStrength.getText().toString();
                String visibility = spinnerVisibility.getSelectedItem().toString();
                String salinity =  spinnerSalinity.getSelectedItem().toString();

                Log.d(TAG, "onClickSaveCatch: "+ catchDateTime + ", " + fish + ", " + weight + ", "
                        + weightMode + ", " + lureUsed + ", " + fishingStyle + ", " + waterType + ", "
                        + waterTemp + ", " + catchDepth + ", " + waterMurkness + ", " + latitude + ", "
                        + longitude + ", " + weather + ", " + weatherTemp + ", " + windStrength + ", " + visibility);

                    Thread thread= new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                String weightMode;
                                if (rbWeightEstimated.isChecked()){
                                    weightMode = "Estimated";
                                }else {
                                    weightMode = "Measured";
                                }
                                String phpUrl = "https://koodiaalto.fi/masterluresPHP/addCatch.php";
                                URL url = new URL(phpUrl);
                                Log.d(TAG, "run: URL: " + url);

                                JSONObject postDataParams = new JSONObject();
                                postDataParams.put("fishType", spinnerFish.getSelectedItem().toString());
                                postDataParams.put("fishWeight", etFishWeigth.getText().toString());
                                postDataParams.put("weightMethod", weightMode);
                                postDataParams.put("lureUsed", lureId);
                                postDataParams.put("locationLat", etLatitude.getText().toString());
                                postDataParams.put("locationLon", etLongitude.getText().toString());
                                postDataParams.put("waterMurkness", spinnerWaterMurkness.getSelectedItem().toString());
                                postDataParams.put("waterTemp", etWaterTemp.getText().toString());
                                postDataParams.put("waterType", spinnerWaterType.getSelectedItem().toString());
                                postDataParams.put("catchDepth", spinnerCatchDepth.getSelectedItem().toString());
                                postDataParams.put("catchMethod", spinnerFishingStyle.getSelectedItem().toString());
                                postDataParams.put("catchDateTime", etDateTime.getText().toString());
                                postDataParams.put("catchTemp", etWeatherTemp.getText().toString());
                                postDataParams.put("catchConditions", spinnerWeather.getSelectedItem().toString());
                                postDataParams.put("catchVisibility", spinnerVisibility.getSelectedItem().toString());
                                postDataParams.put("catchWind", etWindStrength.getText().toString());
                                postDataParams.put("waterSalinity", spinnerSalinity.getSelectedItem().toString());
                                // User ja Country-tiedot syötetään tällä hetkellä suoraan PHP-skriptistä null-arvoilla

                                Log.d(TAG, "run: JSON: " + postDataParams.toString());
                                //Avataan yhteys url:iin
                                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                                httpsURLConnection.setRequestMethod("POST");
                                //Määritelään, että syötetään verkkosivulle dataa.
                                httpsURLConnection.setDoOutput(true);
                                //Noudetaan Outputstream, johon voidaan kirjoittaa BufferedWriterilla dataa.
                                OutputStream outputStream = httpsURLConnection.getOutputStream();
                                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                                //Muutetaan metodilla aikaisemmin luotu JSON-olio string muotoon ja kirjoitetaan se sivulle.
                                bufferedWriter.write(convertJSONtoString(postDataParams));
                                //Suljetaan bufferedWriter, jolloin se myös tyhjentää ennen sulkua kirjoittimen.
                                bufferedWriter.close();
                                //Suljetaan Outputstream.
                                outputStream.close();

                                //Noudetaan Inputstream
                                InputStream stream = httpsURLConnection.getInputStream();

                                //Muutetaan Inputstream string muotoon metodilla.
                                String answer = convertInputStreamToString(stream);
                                Log.d("ASD", answer);

                            }catch (IOException | JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                break;
        }
    }

    private String convertJSONtoString(JSONObject jsonObject)
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = jsonObject.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (first)
            {
                first = false;
            }
            else
            {
                result.append("&");
            }

            try {
                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }

    private String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
             * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
             * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
             * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }

    private void spinnerkokeilu(Spinner spinner, String[] list){
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private String dateTime(){
        GregorianCalendar calender = new GregorianCalendar();
        calender.getTimeZone();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String datetime = dateFormat.format(calender.getTime());
        return datetime;
    }

    private boolean checkAndRequestPermissions(){
        // check permissions granted
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPermissions){
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        // Ask for non-granted permissions
        if ( !listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_CODE);
            return false;
        }
        //App has all permissions. Proceed.
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return true;
    }

    @Override
    public void onMessage(String lat, String lon) {
        Log.d(TAG, "onMessage: " +lat +" " + lon);
        etLatitude.setText(lat);
        etLongitude.setText(lon);
    }

    @Override
    public void onStatusChange(String newStatus) {
    }
}
