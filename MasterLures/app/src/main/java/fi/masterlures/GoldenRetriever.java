package fi.masterlures;

import android.content.res.Resources;
import android.os.AsyncTask;

import java.util.ArrayList;

public class GoldenRetriever extends AsyncTask<String, Integer, Boolean>{

    //Tallennetaan konstruktorille syötettävät parametrit oliolle
    public GoldenRetriever(Resources resources, int wantedResourceId)
    {
        this.resources = resources;
        this.resourceId = wantedResourceId;
    }

    interface RetrievingProgress{
        void retrievingDone(ArrayList dropListItems, int resourceId);
        void onUpdate(Integer percentageDone);
    }

    private RetrievingProgress callback = null;
    private Resources resources = null;
    private int resourceId;
    private ArrayList<String> dropBoxItems = new ArrayList<String>();

    public void setCallback(RetrievingProgress callback)
    {
        this.callback = callback;
    }

    @Override
    protected Boolean doInBackground(String... Strings) {

        try
        {
            //Noudetaan olion resurssista halutulla id:llä StringArray
            String[] wantedResource = resources.getStringArray(resourceId);

            //Asetetaan jokainen StringArrayn itemi ArrayListiin.
            for(int i = 0; i < wantedResource.length; i++)
            {
                dropBoxItems.add(wantedResource[i]);
                publishProgress(new Integer(i));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return new Boolean(true);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    //Palautetaan olion ArrayList ja reusurssin id doInBackgroundin loputtua.
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        callback.retrievingDone(this.dropBoxItems, this.resourceId);
    }
}
