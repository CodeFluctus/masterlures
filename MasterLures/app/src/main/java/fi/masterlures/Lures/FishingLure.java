package fi.masterlures.Lures;

public class FishingLure {
    private String manufacturer;
    private String type;
    private String name;
    private String model;
    private String color;
    private String swimmingDepth;
    private String imageUrl;

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSwimmingDepth() {
        return swimmingDepth;
    }

    public void setSwimmingDepth(String swimmingDepth) {
        this.swimmingDepth = swimmingDepth;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setAllData(String manufacturer, String type, String name, String model, String color, String swimmingDepth, String imageUrl)
    {
        this.manufacturer = manufacturer;
        this.type = type;
        this.name = name;
        this.model = model;
        this.color = color;
        this.swimmingDepth = swimmingDepth;
        this.imageUrl = imageUrl;
    }
}
