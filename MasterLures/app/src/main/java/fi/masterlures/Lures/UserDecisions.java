package fi.masterlures.Lures;

/* *****************************************************************************
 * Tällä luokalla luotuun olioon tallennetaan käyttäjän kaikki tekemät valinnat.
 * Oliosta noutamalla voidaan välittää aina tarvittava tieto eri luokkien
 * metodeille, täten vältetään monimutkaisten metodien muodostamista.
 ******************************************************************************/

public class UserDecisions {
    private String dlManufacturer;
    private String dlType;
    private String dlColor;
    private String dlSwimmingDepth;
    private String dlModel;
    private String dlFishType;
    private String dlSalinity;
    private String dlCatchMethod;

    public String getDlManufacturer() {
        return dlManufacturer;
    }

    public void setDlManufacturer(String dlManufacturer) {
        this.dlManufacturer = dlManufacturer;
    }

    public String getDlType() {
        return dlType;
    }

    public void setDlType(String dlType) {
        this.dlType = dlType;
    }

    public String getDlColor() {
        return dlColor;
    }

    public void setDlColor(String dlColor) {
        this.dlColor = dlColor;
    }

    public String getDlSwimmingDepth() {
        return dlSwimmingDepth;
    }

    public void setDlSwimmingDepth(String dlSwimmingDepth) {
        this.dlSwimmingDepth = dlSwimmingDepth;
    }

    public String getDlModel() {
        return dlModel;
    }

    public void setDlModel(String dlModel) {
        this.dlModel = dlModel;
    }

    public String getDlFishType() {
        return dlFishType;
    }

    public void setDlFishType(String dlFishType) {
        this.dlFishType = dlFishType;
    }

    public String getDlSalinity() {
        return dlSalinity;
    }

    public void setDlSalinity(String dlSalinity) {
        this.dlSalinity = dlSalinity;
    }

    public String getDlCatchMethod() {
        return dlCatchMethod;
    }

    public void setDlCatchMethod(String dlCatchMethod) {
        this.dlCatchMethod = dlCatchMethod;
    }

    public String getDlCatchDepth() {
        return dlCatchDepth;
    }

    public void setDlCatchDepth(String dlCatchDepth) {
        this.dlCatchDepth = dlCatchDepth;
    }

    private String dlCatchDepth;

}
