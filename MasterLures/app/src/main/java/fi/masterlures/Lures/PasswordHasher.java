package fi.masterlures.Lures;

import android.util.Log;

import java.security.SecureRandom;
import java.util.Random;

import de.rtner.misc.BinTools;
import de.rtner.security.auth.spi.PBKDF2Engine;
import de.rtner.security.auth.spi.PBKDF2Parameters;

public class PasswordHasher extends PBKDF2Parameters {

    public PasswordHasher()
    {
        this.passwordSalt = generateRandom64Hex();
    }


    private String passwordSalt;
    private String hashedPassword = null;

    public boolean comparePasswords(String userPassword,String dbPassword, String dbSalt)
    {
        byte[] salt = dbSalt.getBytes();
            //SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);
            PBKDF2Parameters p = new PBKDF2Parameters("HmacSHA256", "UTF-8", salt, 15000);
            byte[] dk = new PBKDF2Engine(p).deriveKey(userPassword);
            String hashedPassword = BinTools.bin2hex(dk);
            Log.d("ASD", "HashedPassword: " +hashedPassword);
            Log.d("ASD", "dbPassword: " +dbPassword);
            if(hashedPassword.equals(dbPassword))
            {
                return true;
            }
            else
            {
                return false;
            }

    }

    private String generateRandom64Hex()
    {
        String hex = "";
        Random rand = new Random();
        for(int i = 0; i < 64; i++)
        {
            int integer = rand.nextInt(16);
            if(integer <= 9)
            {
                hex += String.valueOf(integer);
            }
            else if(integer == 10)
            {
                hex += "A";
            }
            else if(integer == 11)
            {
                hex += "B";
            }
            else if(integer == 12)
            {
                hex += "C";
            }
            else if(integer == 13)
            {
                hex += "D";
            }
            else if(integer == 14)
            {
                hex += "E";
            }
            else if(integer == 15)
            {
                hex += "F";
            }
            else if(integer == 16)
            {
                Log.d("ASD", "ERROR: 16");
            }
        }
        return hex;
    }

    public void generateHashedPassword(String passwordToBeHashed)
    {
        byte[] salt = this.passwordSalt.getBytes();
        PBKDF2Parameters p = new PBKDF2Parameters("HmacSHA256", "UTF-8", salt, 15000);
        byte[] dk = new PBKDF2Engine(p).deriveKey(passwordToBeHashed);
        String hashedPassword = BinTools.bin2hex(dk);
        this.hashedPassword = hashedPassword;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }
}
