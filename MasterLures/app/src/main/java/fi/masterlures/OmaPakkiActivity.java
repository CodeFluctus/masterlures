package fi.masterlures;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import fi.masterlures.Lures.FishingLure;

public class OmaPakkiActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Muistutuksena vaan ensimmäisestä versiosta, ei tarpeellinen muuten.
    int[] IMAGES = {R.drawable.original_floating_f03yp, R.drawable.rattlin_rapala_rnr04sg};
    String[] MANUFACTURERS = {"Rapala", "Rapala"};
    String[] TYPES = {"Vaappu", "Vaappu"};
    String[] MODELS = {"Original Floating", "Rattlin' Rapala"};
    String[] COLORS = {"Yellow Perch", "Silver Gold"};
     */

    FishingLureEngine fishingLureEngine = FishingLureApplication.giveEngine();
    FishingLureEngine fishingLureEngine2 = FishingLureApplication.giveOwnKitEngine();
    FishingLure fishingLure;

    String hasExtras = "Intent { cmp=fi.masterlures/.OmaPakkiActivity (has extras) }";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oma_pakki);
        setTitle("MasterLures - Own kit");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Log.d("DDD", "onCreate: Intentti = " + intent);
        String intentti = intent.toString();

        //Tarkistetaan jos omapakkiin lisätään viehe AddLureToOwnKitActitivyn kautta.
        if(intentti.equals(hasExtras)) {
            int position = 0;
            position = intent.getIntExtra("position", position);
            Log.d("DDD", "onClick: position = " + position);

            fishingLure = fishingLureEngine.giveLure(position);
            fishingLureEngine2.addLure(fishingLure);

            ListView listView = findViewById(R.id.listView_omaPakki);
            CustomAdapter customAdapter = new CustomAdapter();

            listView.setAdapter(customAdapter);
        } else if(fishingLureEngine2 != null){
            ListView listView = findViewById(R.id.listView_omaPakki);
            CustomAdapter customAdapter = new CustomAdapter();

            listView.setAdapter(customAdapter);
        }

        Button buttonAddLure = findViewById(R.id.buttonAddLure);
        buttonAddLure.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAddLure:
                Log.d("OmaPakkiActivity", "onClick: buttonAddLure!");
                Intent intent = new Intent(this, BrowseLures.class);
                startActivity(intent);
                break;
        }
    }

    class CustomAdapter extends BaseAdapter {




        //FishingLure fishingLure = fishingLureEngine.giveLure(position);

        @Override
        public int getCount() {
            return fishingLureEngine2.numberOfLures();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.custom_layout, parent, false);
            }

            ImageView imageView = convertView.findViewById(R.id.imageView);
            TextView textViewManufacturer = convertView.findViewById(R.id.textViewManufacturer);
            TextView textViewType = convertView.findViewById(R.id.textViewType);
            TextView textViewModel = convertView.findViewById(R.id.textViewModel);
            TextView textViewColor = convertView.findViewById(R.id.textViewColor);
            TextView textViewSwimDepth = convertView.findViewById(R.id.textViewSwimDepth);
            Button buttonDeleteFromOmaPakki = convertView.findViewById(R.id.buttonDeleteFromOwnKit);

            fishingLure = fishingLureEngine2.giveLure(position);

            Drawable drawable = getDrawableFromExternalStorage(position);
            if(drawable != null) {
                imageView.setImageDrawable(drawable);
            }


            //imageView.setImageResource(IMAGES[position]);
            textViewManufacturer.setText(fishingLure.getManufacturer());
            textViewType.setText(fishingLure.getType());
            textViewModel.setText(fishingLure.getModel());
            textViewColor.setText(fishingLure.getColor());
            textViewSwimDepth.setText(fishingLure.getSwimmingDepth());

            buttonDeleteFromOmaPakki.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    fishingLureEngine2.deleteLure(fishingLure);
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }
    }

    public Drawable getDrawableFromExternalStorage(int position) {
        fishingLure = fishingLureEngine2.giveLure(position);
        String path = "/storage/emulated/0/Android/data/fi.masterlures/files/lure_images/" + fishingLure.getImageUrl();
        Drawable drawable = Drawable.createFromPath(path);

        /*Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        //Skaalataan kuvaa kokoon 1300px x 900px.
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 1300, 900, true));
        //Palautetaan skaalattu drawable "d".
        return d;*/

        return drawable;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        navigateUpTo();

        /*Context context = getApplicationContext();
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/

        return true;
    }

    private void navigateUpTo() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

