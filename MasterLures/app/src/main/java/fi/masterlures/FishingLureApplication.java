package fi.masterlures;

import android.app.Application;

import fi.masterlures.Lures.RegisterForm;
import fi.masterlures.Lures.UserDecisions;
import fi.masterlures.FishingLureEngine;

public class FishingLureApplication extends Application {
    private final static FishingLureEngine fishingLureEngine = new FishingLureEngine();
    private final static UserDecisions userDecisions = new UserDecisions();
    private final static RegisterForm registerForm = new RegisterForm();
    private final static MasterLuresUser masterLuresUser = new MasterLuresUser();

    //##############################################################################################
    private final static FishingLureEngine fishingLureEngine2 = new FishingLureEngine();

    public static FishingLureEngine giveOwnKitEngine()
    {
        return fishingLureEngine2;
    }
    //##############################################################################################

    public static FishingLureEngine giveEngine()
    {
        return fishingLureEngine;
    }

    public static UserDecisions giveEngineToUserDecisions()
    {
        return userDecisions;
    }

    public static RegisterForm giveRegisterForm()
    {
        return registerForm;
    }

    public static MasterLuresUser giveMasterLuresUser()
    {
        return masterLuresUser;
    }
}
