package fi.masterlures;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import fi.masterlures.Lures.FishingLure;
import fi.masterlures.Lures.RegisterForm;
import fi.masterlures.Lures.UserDecisions;

public class MySQLCommunicator extends AsyncTask <Void, Integer, Boolean>{


    interface MySQLCommunicatorInterface
    {
        void searchingDone();
    }

    public MySQLCommunicator(String wantedUrl)
    {
        this.urlString = wantedUrl;
    }

    private String query, PHPscript;
    private String urlString;
    private MySQLCommunicatorInterface callback = null;

    /************************
    *  GETTERS AND SETTERS! *
    ************************/

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getPHPscript() {
        return PHPscript;
    }

    public void setPHPscript(String PHPscript) {
        this.PHPscript = PHPscript;
    }

    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public void setCallback(MySQLCommunicatorInterface callback)
    {
        this.callback = callback;
    }



    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            //Mikäli oliolle on määritelty käytettävä PHPscripti lisätään se urliin.
            if(PHPscript != null)
            {
                //Mikäli url:in viimeinen merkki ei ole kenoviiva lisätään se ennen
                //PHP-scriptin lisäämistä.
                int lastIndex = urlString.length() - 1;
                if(urlString.charAt(lastIndex) != '/')
                {
                    urlString += "/";
                    Log.d("ASD", "Added /");
                }
                urlString += PHPscript;
            }
            URL url = new URL(urlString);


            //Avataan yhteys url:iin
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            JSONObject postDataParams = new JSONObject();

            if(PHPscript == "customQueryToLures.php")
            {
                //Noudetaan oliosta käyttäjän valinnat puotusvalikoista.
                UserDecisions userDecisions = FishingLureApplication.giveEngineToUserDecisions();

                //Tallennetaan noudetut tiedot JSONObjectille määrätylle key:lle arvoksi.

                postDataParams.put("manuf", userDecisions.getDlManufacturer());
                postDataParams.put("type", userDecisions.getDlType());
                postDataParams.put("color", userDecisions.getDlColor());
                postDataParams.put("depth", userDecisions.getDlSwimmingDepth());

            }
            else if(PHPscript == "registerUser.php")
            {
                RegisterForm registerForm = FishingLureApplication.giveRegisterForm();

                postDataParams.put("firstname", registerForm.getFirstname());
                postDataParams.put("lastname", registerForm.getLastname());
                postDataParams.put("username", registerForm.getUsername());
                postDataParams.put("country", registerForm.getCountry());
                postDataParams.put("email", registerForm.getEmail());
                postDataParams.put("salt", registerForm.getSalt());
                postDataParams.put("hashedPassword", registerForm.getHashedPassword());
            }
            else if(PHPscript == "loginUser.php")
            {
                MasterLuresUser masterLuresUser = FishingLureApplication.giveMasterLuresUser();
                postDataParams.put("username", masterLuresUser.getUsername());
            }
            //Määritellään pyyntimetodiksi POST
            httpsURLConnection.setRequestMethod("POST");
            //Määritelään, että syötetään verkkosivulle dataa.
            httpsURLConnection.setDoOutput(true);

            //Noudetaan Outputstream, johon voidaan kirjoittaa BufferedWriterilla dataa.
            OutputStream outputStream = httpsURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

            //Muutetaan metodilla aikaisemmin luotu JSON-olio string muotoon ja kirjoitetaan se sivulle.
            bufferedWriter.write(convertJSONtoString(postDataParams));

            //Suljetaan bufferedWriter, jolloin se myös tyhjentää ennen sulkua kirjoittimen.
            bufferedWriter.close();
            //Suljetaan Outputstream.
            outputStream.close();

            //Noudetaan Inputstream
            InputStream stream = httpsURLConnection.getInputStream();

            //BufferedInputStream bufferedInputStream = new BufferedInputStream(stream);
            //baStream = new ByteArrayInputStream();
            //Log.d("ASD", "Yhteys avattu!");
            //Log.d("ASD", "available: " + stream.available() );
            //Log.d("ASD", "available: " + stream.read() );
            /*while(stream.available() > 0)
            {
                Log.d("ASD", "byte:" + stream.read());
            }*/
            //Log.d("ASD", "Valmis!");


            //Muutetaan Inputstream string muotoon metodilla.
            String answer = convertInputStreamToString(stream);
            Log.d("ASD", answer);

            if(PHPscript == "customQueryToLures.php")
            {
                //Lisätään kyseisestä streamista luret applikaation muistiin.
                addLuresToStack(answer);
            }
            else if(PHPscript == "registerUser.php")
            {
                checkDatabaseUpdateForErrors(answer);
            }
            else if(PHPscript == "loginUser.php")
            {
                getUserLoginData(answer);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private String convertJSONtoString(JSONObject jsonObject)
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = jsonObject.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (first)
            {
                first = false;
            }
            else
            {
                result.append("&");
            }

            try {
                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result.toString();

        //return stringToReturn;
    }

    private String convertInputStreamToString(InputStream inputStream)
    {
        String stringToReturn = "";
        try{
            /*Luetaan ensimmäinen merkki. Tällä saadaan myös käynnistymään available-metodi
            * Ilman tätä väittää saatavilla olevien merkkien määrän 0:si;
            * Koska read-metodi palauttaa ASCII-koodatun kokonaisluvun muutetaan se Character
            * olion avulla char-merkiksi ja tallennetaan stringiin.*/
            stringToReturn += Character.toString((char)inputStream.read());

            //Luetaan merkkejä Stringiin niin pitkään kuin merkkejä on saatavilla.
            while (inputStream.available() > 0)
            {
                stringToReturn += Character.toString((char)inputStream.read());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //Palautetaan luotu stringi.
        return stringToReturn;
    }

    private void addLuresToStack(String stringOfJSONObjects)
    {
        //Noudetaan lista kaikista uistimista. Tyhjennetään lista, edellisestä hausta.
        FishingLureEngine fishingLureEngine = FishingLureApplication.giveEngine();
        fishingLureEngine.clearAllLures();

        try {
            //Luodaan uusi JSON-olio, johon luetaan sisään syötetty string
            JSONObject jsonObject = new JSONObject(stringOfJSONObjects);

            //Etsitään luodusta oliosta array olioista nimellä "lure".
            JSONArray lureArray = jsonObject.getJSONArray("lure");

            /*Elementti elementiltä haetaan kysiesen elementin tiedot ja
            * tallennetaan ne muuttujiin. Syötetään noudetut tiedot
            * uistimelle. Lisätään uuden uistimen tiedot kaikkien
            * uistimien ArrayList:iin.
            */
            for (int i = 0; i < lureArray.length(); i++)
            {
                JSONObject lureObject = lureArray.getJSONObject(i);

                String manufacturer = lureObject.getString("manufacturer");
                String type = lureObject.getString("type");
                String name = lureObject.getString("name");
                String model = lureObject.getString("model");
                String color = lureObject.getString("color");
                String depth = lureObject.getString("depth");
                String imageUrl = lureObject.getString("imageUrl");

                FishingLure fishingLure = new FishingLure();
                fishingLure.setAllData(manufacturer, type, name, model, color, depth, imageUrl);

                fishingLureEngine.addLure(fishingLure);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void checkDatabaseUpdateForErrors(String streamFromUrl)
    {
        RegisterForm registerForm = FishingLureApplication.giveRegisterForm();

        /*Yritetään noutaa arvoa keylle error. Tätä ei kuitenkaan palaudu
        * OLLENKAAN mikäli php-scriptissä ei ilmaannu virhettä, jolloin
        * "joudutaan" suorittamaan exception tässä sovelluksessa.*/
        try
        {
            JSONObject mainJsonObject = new JSONObject(streamFromUrl);
            JSONObject userJsonObject = mainJsonObject.getJSONObject("user");
            Log.d("ASD", "JSON: " + userJsonObject.toString());
            String error = userJsonObject.getString("error");
            Log.d("ASD", "JSONError: " + error);
            registerForm.setDbError(error);
        }
        /*Eli php-scriptissä ei erroria = Error sovelluksessa.*/
        catch (Exception e)
        {
            e.printStackTrace();
            registerForm.setDbError(null);
        }
    }

    private void getUserLoginData(String streamFromUrl)
    {
        MasterLuresUser masterLuresUser = FishingLureApplication.giveMasterLuresUser();
        Log.d("ASD", "getUserLoginData, username: " + masterLuresUser.getUsername());
        try
        {
            JSONObject mainJsonObject = new JSONObject(streamFromUrl);
            JSONObject userJsonObject = mainJsonObject.getJSONObject("user");
            Log.d("ASD", "JSON: " + userJsonObject.toString());
            String password = userJsonObject.getString("password");
            String salt = userJsonObject.getString("salt");
            masterLuresUser.setPasswordFromDB(password);
            masterLuresUser.setSaltFromDB(salt);
            try
            {
                String error = userJsonObject.getString("error");
                Log.d("ASD", "JSONError: " + error);
                masterLuresUser.setDbError(error);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                masterLuresUser.setDbError(null);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        Log.d("ASD", "Password from DB: " + masterLuresUser.getPasswordFromDB());
        Log.d("ASD", "Salt from DB: " + masterLuresUser.getSaltFromDB());
    }


    //Ilmoitetaan käyttöliittymälle rajapinnan avulla, että haku on suoritettu.
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        callback.searchingDone();
    }
}
