package fi.masterlures.AWSS3;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fi.masterlures.FishingLureApplication;
import fi.masterlures.FishingLureEngine;
import fi.masterlures.Lures.FishingLure;

public class Download {
    // The S3 client used for getting the list of objects in the bucket
    private AmazonS3Client s3;
    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    private Util util;
    private String bucket = "masterlures";

    //Luodaan uusi TransferUtility-olio aws:sää varten
    static TransferUtility transferUtility;

    private String photoName;

    private static final String TAG = "Download";

    //Tarvitaan tietyn fishingLuren URL kuvien lataamiseen.
    private FishingLureEngine fishingLureEngine;
    private FishingLure fishingLure;

    public void startDownload(Context context) {
        util = new Util();

        fishingLureEngine = FishingLureApplication.giveEngine();

        //transferRecordMaps = new ArrayList<HashMap<String, Object>>();
        transferUtility = util.getTransferUtility(context);
        initData(context);

        for (int i = 0; i < fishingLureEngine.numberOfLures(); i++) {
            fishingLure = fishingLureEngine.giveLure(i);

            String downloadURL = "lure_images/" + fishingLure.getImageUrl();
            beginDownload(context ,downloadURL);

            Log.d(TAG, "startDownload: " + downloadURL);

            /**
             * 1	Rapala	Plug	Team Esko	Orange	    Shallow	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/team_esko_07_rapala_lures_te07_oprt_hero.jpg
             * 2	Rapala	Plug	Original	Brown	    Shallow	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/countdown_07_rapala_lures_cd07_trl_hero.jpg
             * 4	Rapala	Plug	Magnum	    White	    Deep	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/countdown_magnum_18_rapala_lures_cdmag18_rh_hero.jpg
             * 5	Rapala	Plug	Magnum	    Green	    Deep	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/husky_magnum_15_rapala_lures_hmag15_d_hero.jpg
             * 6	Rapala	Plug	Husky	    Multicolor	Medium  https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/scatter_rap_husky_13_rapala_lures_scrh13_hfsbr_hero.jpg
             * 7	Rapala	Plug	Original	Grey	    Surface	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/original_floater_11_rapala_lures_f11_s_hero.jpg
             * 8	Rapala	Plug	Original	Orange	    Surface	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/mini_fat_rap_03_rapala_lures_mfr03_gfr_hero.jpg
             * 9	Rapala	Plug	Original	Green	    Surface	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/ultra_light_shad_04_rapala_lures_uls04_ft_hero.jpg
             * 10	Rapala	Plug	Long Cast	Blue	    Surface	https://masterlures.s3.eu-central-1.amazonaws.com/lure_images/x-rap_long_cast_12_rapala_lures_sxrl12_bsrd_hero.jpg
             */
        }
        //GetFileListTask getFileListTask = new GetFileListTask();
        //getFileListTask.execute();
    }

    private void initData(Context context) {
        // Gets the default S3 client.
        s3 = util.getS3Client(context);
    }

    private void beginDownload(Context context, String key) {
        // Location to download files from S3 to. You can choose any accessible
        //Log.d("testi", key);

        /**
         * Käytetään ladattaessa yhtä kuvaa.
         */

        File file = new File(context.getExternalFilesDir(null), key);

        String path = file.toString();
        Log.d(TAG, "beginDownload: Path to downloaded images: " + path);

        //Ladataan keyn mukainen tiedosto edellä asetettuun tiedostosijaintiin.
        TransferObserver observer = transferUtility.download(key, file, transferListener);

        //Tallennetaan kuvan nimi key:stä ja asetetaan se näkyviin showImage-metodilla.
        photoName = key;
    }

    /**
     * This async task queries S3 for all files in the given bucket so that they
     * can be displayed on the screen
     */
    private class GetFileListTask extends AsyncTask<Void, Void, Void> {
        // The list of objects we find in the S3 bucket
        private List<S3ObjectSummary> s3ObjList;
        // A dialog to let the user know we are retrieving the files

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... inputs) {
            // Queries files in the bucket from S3.
            s3ObjList = s3.listObjects(bucket).getObjectSummaries();
            transferRecordMaps.clear();
            for (S3ObjectSummary summary : s3ObjList) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("key", summary.getKey());
                transferRecordMaps.add(map);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            for (int i = 0; i < transferRecordMaps.size(); i++) {
                String omaatestiatestiatestia = transferRecordMaps.get(1).toString();
                Log.d(TAG, "onPostExecute: " + omaatestiatestiatestia);
            }

        }
    }

    TransferListener transferListener = new TransferListener() {
        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d(TAG, "onStateChanged: " + state.toString());
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, "onProgressChanged: bytesTotal: " + bytesTotal + " / " + bytesCurrent);
        }

        @Override
        public void onError(int id, Exception ex) {
            Log.d(TAG, "onError: ERROR!");
            ex.getCause();
            ex.printStackTrace();
        }
    };
}
