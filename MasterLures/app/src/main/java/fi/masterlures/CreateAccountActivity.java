package fi.masterlures;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Random;
import fi.masterlures.Lures.PasswordHasher;
import fi.masterlures.Lures.RegisterForm;

public class CreateAccountActivity extends AppCompatActivity implements View.OnClickListener, MySQLCommunicator.MySQLCommunicatorInterface, AdapterView.OnItemSelectedListener, GoldenRetriever.RetrievingProgress {

    private EditText etPassword, etPasswordRepeat, etFirstname, etLastname, etEmail, etUsername;
    Spinner spinnerCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        findViewById(R.id.buttonCancel).setOnClickListener(this);
        findViewById(R.id.buttonRegister).setOnClickListener(this);
        setTitle("MasterLures - Create account");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        etPassword = findViewById(R.id.etPassword);
        etPasswordRepeat = findViewById(R.id.etPasswordRep);
        etFirstname = findViewById(R.id.etFirstname);
        etLastname = findViewById(R.id.etLastname);
        etEmail = findViewById(R.id.etEmail);
        etUsername = findViewById(R.id.etUsername);

        spinnerCountry = findViewById(R.id.spinnerCountry);
        spinnerCountry.setOnItemSelectedListener(this);

        GoldenRetriever goldenRetriever = new GoldenRetriever(getResources(), R.array.countryName);
        goldenRetriever.setCallback(this);
        goldenRetriever.execute("");
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.buttonCancel)
        {
            onBackPressed();
            Toast.makeText(this, "Account creation cancelled!", Toast.LENGTH_SHORT).show();
        }
        if(view.getId() == R.id.buttonRegister)
        {
            String p1 = etPassword.getText().toString();
            String p2 = etPasswordRepeat.getText().toString();
            String email = etEmail.getText().toString();

            Log.d("ASD", "p1: " + p1 + ", p2: " +p2);
            if(p1.equals(p2))
            {
                //Email regex
                String emailRegex = "(^[\\w.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]{1,3}+(\\.[\\w]{1,3}+$)?+$)";
                if(email.matches(emailRegex)) {
                    /*
                     * Vaaditaan käyttäjältä vähintään yksi
                     * - iso kirjain
                     * - pieni kirjain
                     * - numero
                     * - merkki, Hyväksytyt merkit ovat: !"#¤%&/()=?,.
                     * Salasanan pituus on oltava 8-25
                     * */
                    String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[!\\/#¤%()=?,.])[a-zA-Z\\d!\\/#¤%()=?,.]{8,25}";


                    if (p1.matches(regex)) {

                        MySQLCommunicator mySQLCommunicator = new MySQLCommunicator("https://koodiaalto.fi/masterluresPHP/");
                        mySQLCommunicator.setPHPscript("registerUser.php");
                        mySQLCommunicator.setCallback(this);

                        PasswordHasher passwordHasher = new PasswordHasher();
                        passwordHasher.generateHashedPassword(p1);

                        RegisterForm registerForm = FishingLureApplication.giveRegisterForm();
                        registerForm.setFirstname(etFirstname.getText().toString());
                        registerForm.setLastname(etLastname.getText().toString());
                        registerForm.setUsername(etUsername.getText().toString());
                        registerForm.setCountry(spinnerCountry.getSelectedItem().toString());
                        registerForm.setEmail(etEmail.getText().toString());
                        registerForm.setHashedPassword(passwordHasher.getHashedPassword());
                        registerForm.setSalt(passwordHasher.getPasswordSalt());

                        Log.d("ASD", "Hashed password: " + passwordHasher.getHashedPassword());
                        Log.d("ASD", "END");
                        mySQLCommunicator.execute();
                    } else {
                        Toast.makeText(this, "Password doesn't meet the requirements!", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(this,"Email isn't valid!", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(this,"Passwords doesn't match eachother! Try again!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*private boolean checkPasswordRequirments(String password)
    {

            String regex = "[a-zA-Z0-9]{8,}";
            if(password.matches(regex))
            {
                Log.d("ASD", "Määräykset toteutuvat!");
                return true;
            }
            else
            {
                Log.d("ASD", "Ei täytä määräyksiä!");
            }
        }
        return false;
    }*/
    private String generateRandom64Hex()
    {
        String hex = "";
        String nextChar;
        Random rand = new Random();
        for(int i = 0; i < 64; i++)
        {
            int integer = rand.nextInt(16);
            if(integer <= 9)
            {
                hex += String.valueOf(integer);
            }
            else if(integer == 10)
            {
                hex += "A";
            }
            else if(integer == 11)
            {
                hex += "B";
            }
            else if(integer == 12)
            {
                hex += "C";
            }
            else if(integer == 13)
            {
                hex += "D";
            }
            else if(integer == 14)
            {
                hex += "E";
            }
            else if(integer == 15)
            {
                hex += "F";
            }
            else if(integer == 16)
            {
                Log.d("ASD", "ERROR: 16");
            }


        }

        return hex;
    }

    @Override
    public void searchingDone() {
        RegisterForm registerForm = FishingLureApplication.giveRegisterForm();
        if(registerForm.getDbError() == null)
        {
            Toast.makeText(this,"New user created successfully!\nUsername:" + registerForm.getUsername(), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        else
        {
            Toast.makeText(this,"Error on creating new user\nErrorcode: " + registerForm.getDbError(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ASD", "Itemi valittu:" + i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //Suoritetaan kun GoldenRetriever on suorittanut doInBackground-metodinsa ja kutsuu onPostExecute-metodiaan
    @Override
    public void retrievingDone(ArrayList dropListItems, int resourceId) {
        //Määritellään adapteri DropDownViewille ja asetetaan sille näkymäresurssi
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dropListItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Päätetään oliolta saatavan parametrin mukaan, että mille spinnerille asetetaan näkymään
        //siltä saatu ArrayList.
        if(resourceId == R.array.countryName)
        {
            spinnerCountry.setAdapter(adapter);
        }
    }

    @Override
    public void onUpdate(Integer percentageDone) {

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return true;
    }
}
