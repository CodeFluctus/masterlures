<?php

    require "connectDB.php";
    $manufacturer = trim(htmlspecialchars(strip_tags($_POST["manuf"])));
    $type = trim(htmlspecialchars(strip_tags($_POST["type"])));
    $color = trim(htmlspecialchars(strip_tags($_POST["color"])));
    $swimmingDepth = trim(htmlspecialchars(strip_tags($_POST["depth"])));

    $queryString = "Select * from lure where";

    /*echo "Man: " . $manufacturer . "<br>";
    echo "type: " . $type . "<br>";
    echo "color: " . $color . "<br>";
    echo "sd: " . $swimmingDepth . "<br>";*/

    if ($manufacturer != "All") {
        $queryString .=" lure_manufacturer='" . $manufacturer . "' and";
    }
    if ($type != "All") {
        $queryString .=" lure_type='" . $type . "' and";
    }
    if ($color != "All") {
        $queryString .=" lure_color='" . $color . "' and";
    }
    if ($swimmingDepth != "All") {
        $queryString .=" lure_depth='" . $swimmingDepth . "' and";
    }

    if($queryString == "Select * from lure where")
    {
        $queryString = "Select * from lure";
    }
    else
    {
        $queryString = substr($queryString, 0, (strlen($queryString) - 4));
    }

    //echo "$queryString" ."<br><br>";

    $sql = $dbConnection->prepare($queryString);
    $sql->execute();

    //$result = $sql->fetchAll();
    //print_r($result);

	$lureArray = array();
    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
		
		$lureObject = new stdClass();
        $lureObject->id = $row['lure_id'];
        $lureObject->manufacturer =$row['lure_manufacturer'];
        $lureObject->type = $row['lure_type'];
        $lureObject->name = $row['lure_name'];
        $lureObject->model = $row['lure_model'];
        $lureObject->color = $row['lure_color'];
        $lureObject->depth = $row['lure_depth'];
        $lureObject->imageUrl = $row['lure_image_url'];
		
		
        //$lureJSON = json_encode($lureObject);
		
		array_push($lureArray,$lureObject);
		
        //echo "<pre>";
        //echo $lureJSON;
        //echo "</pre>";
        /*echo "ID: " .$row['lure_id'] . "<br>";
        echo "Manufacturer: " .$row['lure_manufacturer'] . "<br>";
        echo "Type: " .$row['lure_type'] . "<br>";
        echo "Name: " .$row['lure_name'] . "<br>";
        echo "Model: " .$row['lure_model'] . "<br>";
        echo "Color: " .$row['lure_color'] . "<br>";
        echo "Depth: " .$row['lure_depth'] . "<br>";
        echo "ImageUrl: " .$row['lure_image_url'] . "<br><br>";*/
    }
    $mainObject = new stdClass();
	$mainObject->lure = $lureArray;
	$mainObjectJSON = json_encode($mainObject);
	echo $mainObjectJSON;

?>