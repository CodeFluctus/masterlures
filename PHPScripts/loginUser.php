<?php

    require "connectDB.php";

    $username = trim(htmlspecialchars(strip_tags($_POST["username"])));

    $queryString = "SELECT password, salt from user WHERE username = ?";

    

    $userObject = new stdClass();

    try 
    {
        $sql = $dbConnection->prepare($queryString);
        $sql->execute(array($username));
    } 
    catch (PDOException $e) {
        
        if($e == null)
        {
            echo "Ei häikkää <br>";
            //echo $e . "<br>";
            $userObject->error = null;
        }
        else
        {
            //echo "ERROR: <br>";
            //echo $e . "<br>";
            $userObject->error = $e->getCode();
            //echo $e->getMessage() . "<br>";
        }
    }

    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        $userObject->password = $row['password'];
        $userObject->salt = $row['salt'];
    }
    $mainObject = new stdClass();
    $mainObject->user = $userObject;
    $JSONmainObject = json_encode($mainObject);
    echo $JSONmainObject;
?>