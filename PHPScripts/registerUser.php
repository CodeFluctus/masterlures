<?php

    require "connectDB.php";

    $firstname = trim(htmlspecialchars(strip_tags($_POST["firstname"])));
    $lastname = trim(htmlspecialchars(strip_tags($_POST["lastname"])));
    $username = trim(htmlspecialchars(strip_tags($_POST["username"])));
    $country = trim(htmlspecialchars(strip_tags($_POST["country"])));
    $email = trim(htmlspecialchars(strip_tags($_POST["email"])));
    $hashedPassword = trim(htmlspecialchars(strip_tags($_POST["hashedPassword"])));
    $salt = trim(htmlspecialchars(strip_tags($_POST["salt"])));

    $userObject = new stdClass();

    $userObject->firstname = $firstname;
    $userObject->lastname = $lastname;
    $userObject->username = $username;
    $userObject->country = $country;
    $userObject->email = $email;
    $userObject->hashedPassword = $hashedPassword;
    $userObject->salt = $salt;

    

    try 
    {
        $sql = $dbConnection->prepare("INSERT INTO user VALUES(NULL, ?, ?, ?, ?, ?, ?, NULL, ?)");
    $sql->execute(array($firstname, $lastname, $username, $email, $hashedPassword, $salt, $country));
    } catch (PDOException $e) {
        
        if($e == null)
        {
            echo "Ei häikkää <br>";
            //echo $e . "<br>";
            $userObject->error = null;
        }
        else
        {
            //echo "ERROR: <br>";
            //echo $e . "<br>";
            $userObject->error = $e->getCode();
            //echo $e->getMessage() . "<br>";
        }
    }

    $mainObject = new stdClass();
    $mainObject->user = $userObject;
    $mainObjectJSON = json_encode($mainObject);

    echo $mainObjectJSON;

    

    

    //print_r($salt);

    function createSalt()
    {
        //declare(encoding='UTF-8');
        $salt = "";
        for($i = 0; $i < 5; $i++)
        {
            //Salt muodostuu ASCII:n merkeistä väliltä [33, 126]
            $randInt = rand(33,126);
            $char = chr($randInt);
            echo "randInt: " . $randInt . " = char: " . $char . "<br>";
            $salt .= $char;
        }
        return $salt;
    }

?>