<?php

    require "connectDB.php";


    /*Luetaan käyttäjän syöttämät tiedot ja tehdään niille tarvittavat suodattamiset
    * mysql-injektointien varalta.*/
    $fishType = trim(htmlspecialchars(strip_tags($_POST["fishType"])));
    $fishWeight = (float) $_POST["fishWeight"]; //!!!!!!!!!!!!!!! float
    $fishWeightMethod = trim(htmlspecialchars(strip_tags($_POST["weightMethod"])));
    //$lureUsed = trim(htmlspecialchars(strip_tags($_POST["lureUsed"]))); // id?
	$lureUsed = (int) $_POST["lureUsed"]; // id?
    $locationLat = doubleval($_POST["locationLat"]); // double
    $locationLon = doubleval($_POST["locationLon"]); // double
    $waterSalinity = trim(htmlspecialchars(strip_tags($_POST["waterSalinity"]))); 
    $waterMurkness = trim(htmlspecialchars(strip_tags($_POST["waterMurkness"]))); 
    $WaterTemp = intval($_POST["waterTemp"]);
    $WaterType = trim(htmlspecialchars(strip_tags($_POST["waterType"])));
    $catchDepth = trim(htmlspecialchars(strip_tags($_POST["catchDepth"])));
    $catchMethod = trim(htmlspecialchars(strip_tags($_POST["catchMethod"]))); 
    $catchDateTime = new DateTime(trim(htmlspecialchars(strip_tags($_POST["catchDateTime"])))); 
    $weatherTemp = (int) $_POST["catchTemp"]; 
    $weatherConditions = trim(htmlspecialchars(strip_tags($_POST["catchConditions"]))); 
    $weatherVisibility = trim(htmlspecialchars(strip_tags($_POST["catchVisibility"]))); 
    $weatherWind = (int) $_POST["catchWind"];

    echo $locationLon . $locationLat;

    echo "<br>" . $catchDateTime->format("d-m-Y H:i:s") . "<br><br>";

    

    $dbAcceptedInputs = getAcceptedInputs($dbConnection);

    



    /** catch_a_fish table (18 elements):
     * id(NN,AI), fish_type(NN), fish_weight, catch_fish_weight_method,
     * lure_used(NN), catch_location_lat, catch_location_lon, catch_water_salinity,
     * catch_water_murkness, catch_water_tempereature, catch_water_type, catch_depth,
     * catch_method, catch_date_time(NN), catch_weather_temp, catch_weather_condition,
     * catch_weather_visibility, catch_weather_wind
     */

    
                    
    $userInputs = array();

    foreach ($dbAcceptedInputs as $key => $value) {
        if($key == "catch_fish_weight_method")
        {
            $userInputs[$key] = /*'Estimated';//*/$fishWeightMethod;
        }
        elseif ($key == "catch_water_salinity") 
        {
            $userInputs[$key] = /*"Brackish";//*/$waterSalinity;
        }
        elseif ($key == "catch_water_murkness") 
        {
            $userInputs[$key] = /*"Murky";//*/$waterMurkness;
        }
        elseif ($key == "catch_water_type") 
        {
            $userInputs[$key] = /*"Lake";//*/$WaterType;
        }
        elseif ($key == "catch_depth") 
        {
            $userInputs[$key] = /*"Surface";//*/$catchDepth;
        }
        elseif ($key == "catch_method") 
        {
            $userInputs[$key] = /*"Throwing";//*/$catchMethod;
        }
        elseif ($key == "catch_weather_conditions") 
        {
            $userInputs[$key] = /*"Sunny";//*/$weatherConditions;
        }
        elseif ($key == "catch_weather_visibility") 
        {
            $userInputs[$key] = /*"Darkness";//*/$weatherVisibility;
        }
    }

    foreach ($userInputs as $userkey => $uservalue) {

        //Debugging
        //echo "Userkey: " . $userkey . "<br>";
        foreach ($dbAcceptedInputs as $dbkey => $dbvalue) {

            //Debugging
            //echo "dbKEY: " . $dbkey . "<br>";
            if($userkey == $dbkey)
            {
                //Debugging
                //echo "MATCH!: " . $userkey . " = " . $dbkey . "<br>";
                
                /* Tutkitaan array_search-metodilla löytyykö $uservalue $dbvalue-arraysta. 
                *  Array_search palauttaa arvon booleanina ja tutkitaan vastaako arvo
                *  arvoa false. Mikäli näin on muutetaan arvo NULL:iksi.
                 */
                if(array_search($uservalue, $dbvalue) === false)
                {
                    /* //Debugging

                    echo "array_search Type: " . gettype(array_search($uservalue, $dbvalue)) . ",array_search Value: " . array_search($uservalue, $dbvalue) . "<br>";
                    echo "Not right format! Nulling userinput with key: " . $userkey . ", Value: " . $uservalue . "<br>";
                    echo "Should be: ";
                    */for($i = 0; $i < count($dbvalue); $i++)
                    {
                        echo $dbvalue[$i] . "(" . strlen($dbvalue[$i]) . ")" . "(" . gettype($dbvalue[$i]) . "), ";
                    }
                    echo "<br>But you have: " . $uservalue  . "(" . strlen($uservalue) . ")" . "(" . gettype($uservalue) . ")<br><br>";
                    /**/
                    $userInputs[$userkey] = NULL;
                }
                /*  //Debugging
                else
                {
                    echo "Type: " . gettype(array_search($uservalue, $dbvalue)) . ",Value: " . array_search($uservalue, $dbvalue);
                    echo "Everything ok! <br><br>";
                }
                */
                break;
            }
        }
    }

    foreach ($userInputs as $key => $value) {
        echo $key . "-> " . $value . "<br>";
    }

    $sql = $dbConnection->prepare("SELECT fish_id FROM fish WHERE fish_name= ?");
    $sql->execute(array($fishType));
    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        //echo "ASD";
        echo $row['fish_id'];
        $fishType= $row['fish_id'];
    }



    $queryString = "INSERT INTO catch_a_fish VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $sql = $dbConnection->prepare($queryString);

    


$lureUsed += 1;

    $sql->execute(array(NULL,$fishType, $fishWeight, $userInputs["catch_fish_weight_method"], 
                    $lureUsed, $locationLat, $locationLon, $userInputs["catch_water_salinity"],
                    $userInputs["catch_water_murkness"], $WaterTemp, $userInputs["catch_water_type"], $userInputs["catch_depth"],
                    $userInputs["catch_method"], $catchDateTime->format("Y-m-d H:i:s"), $weatherTemp, $userInputs["catch_weather_conditions"],
                    $userInputs["catch_weather_visibility"], $weatherWind, NULL, NULL));

                    

    print_r($dbAcceptedInputs);


    
    //Vaaditaan parametrina yhteysolio, jotta voidaan suorittaa kyseisen olion metodeja.
    function getAcceptedInputs($_dbConnection)
    {
        //Valmistellaan query ja suoritetaan se
        $_queryString = "DESC catch_a_fish";

        $_sql = $_dbConnection->prepare($_queryString);
        $_sql->execute();

        //Luodaan array, johon tullaan tallentamaan tietokannassa olevat hyväksyttävät inputit.
        $_dbAcceptedArrays = array();

        //Noudetaan tietoja rivi riviltä kunnes palautuu null
        while($row = $_sql->fetch(PDO::FETCH_ASSOC))
        {
            /* //Debugging
            echo "{$row['Field']} - {$row['Type']}\n" . "<br>";
            */


            /*Mikäli kyseinen Type sisältää datatyypin enum tallennetaan se
            $dbAcceptedArrays:iin*/

            /**
             * Tarkastellaan preg_match-funktiolla $row['Type'] arvoa.
             * preg_match("etsittävä_merkkijono", "mistä_etsitään")
             * ^------ Tässä muodossaan palauttaa arvon true tai false
             * 
             * Seulontaan käytetään regex:in määrittelemää merkkijonoa.
             * Käytettyn Regex (Regular expression)-syntaksin selitys:
             * "/ ja /" Aloitus ja lopetus merkki.Kyseisten merkkien sisään 
             *          kirjoitetaan Regex-lauseke.
             *    ^     merkkijonon aloituskohta. Eli tässä tapauksessa aloitetaan 
             *          lukeminen kohdalta kun enum esiintyy ensimmäisen kerran
             *          $row['Type'] palauttamassa stringissä.
             *    \     jälkeen tulevaa merkkiä käsitellään juuri sellaisena kuin
             *          se on Regex lauseessa kirjoitettu.
             *    .     Tarkoittaa kaikkia merkkejä
             *    *     Tarkoittaa 0 <= merkkejä
             *    $     merkkijonon lopetuskohta. Eli tarkasteltava merkkijono päättyy
             *          merkkiin ) tässä tapauksessa.
             * 
             * Suomeksi: Etsitään $row['Type']:stä seuraavanlaisen parametrin 
             * täyttävää merkkijonoa:
             * enum('mitäTahansaTekstiäJaMinkäTahansaPituista')
             * 
             * HOX!! Välissä voi olla myös mitä tahansa merkkejä, eli voi myös olla muotoa:
             * enum('item_1', 'item_2', ... , 'item_n')
             *      
             * Lisää aiheesta:
             * https://www.php.net/manual/en/function.preg-match.php
             * https://www.guru99.com/php-regular-expressions.html
             */

            if(preg_match("/^enum\(\'(.*)\'\)$/", $row['Type']))
            {
                /* //Debugging
                echo "True:" . $row['Field'] . "<br><br>";
                */

                preg_match("/^enum\(\'(.*)\'\)$/", $row['Type'], $matches);
                /**Tässä muodossa palauttaa $matches-arrayn. Arrayn eka elementti sisältää
                 * koko regex-lausekkeen määritteen.
                 * Toinen elementti sisältää (.*) määritteen mukaisesti kaikki elementit
                 * pilkuilla eroteltuina.
                 * */

                /* //Debugging
                print_r($matches);
                */

                //Hajoitetaan pilkulla erotellut elementit itseisiksi arrayn elementeiksi
                $items = explode("','", $matches[1]);

                //Luodaan dbAcceptedArrays:iin $row['Field'] keylle array, johon luetaan
                //tiedot $items arraysta.
                $_dbAcceptedArrays[$row['Field']] = array();
                for ($i=0; $i < count($items); $i++) 
                { 
                    array_push($_dbAcceptedArrays[$row['Field']], $items[$i]);
                }
            }
        }
        return $_dbAcceptedArrays;
    }
?>