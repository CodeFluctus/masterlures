<?php

    require "connectDB.php";

    /*

    *************POISTOON*************

    $searchObject = new Searcher();

    $searchObject->setUserId(12);
    //$fishType = trim(htmlspecialchars(strip_tags($_POST["fishType"])));

    //$fishType = "Pike";
    
        *************POISTOON*************
    
    */

    /*Noudetaan tietokannasta kaikki kalalajien nimet arrayhin ja niiden
    id:t vastaaviksi key arvoiksi*/

    $fishTypeIdArray = array();
    
    $sql = $dbConnection->prepare("Select * from fish");
    $sql->execute();
    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        //echo "Type: " . $row['fish_id'] . ", Field: " . $row['fish_name'];
        //echo "<br>";
        $fishTypeIdArray[$row['fish_id']] = $row['fish_name'];
    }


    /* Määritellään array, johon luetaan käyttäjän syöttämät tiedot. */

    $userInputs = array();
    $userInputs["catch_method"] = trim(htmlspecialchars(strip_tags($_POST["catchMethod"])));
    $userInputs["lure_used"] = trim(htmlspecialchars(strip_tags($_POST["lureUsed"])));
    $userInputs["catch_water_type"] = trim(htmlspecialchars(strip_tags($_POST["waterType"])));
    $userInputs["fish_type"] = array_search(trim(htmlspecialchars(strip_tags($_POST["fishType"]))), $fishTypeIdArray);
    $userInputs["user_id"] = (int)$_POST["userId"];

    

    
/*

    //HARDCODED INPUTS




    $searchObject->setFishTypeId(getFishtypeIdFor($fishType, $dbConnection));

    $searchObject->setFishingStyle("Throwing");
    $searchObject->setLureType("Plug");
    $searchObject->setWaterType("River");*/
    //$searchObject->setFishType("Pike")
    //$searchObject->setUserId(trim(htmlspecialchars(strip_tags($_POST["userId"]))));
    
    /*// Debugging
    echo $searchObject->getUserId() . "<br>";
    echo $searchObject->getFishTypeId() . "<br>";*/


    //$queryString = "SELECT * FROM catch_a_fish";

    

    /*$testArray = array();
    $testArray["catch_method"] = "Throwing";
    $testArray["lure_used"] = NULL;
    $testArray["catch_water_type"] = NULL;
    $testArray["user_id"] = NULL;
    
    *************POISTOON*************
    
    */

    /* Noudetaan enumien sallimat syötteet */
    $dbAcceptedInputs = getAcceptedInputs($dbConnection);


    /* Käydään elementti elementiltä lävitse, että vastaako käyttäjän syöttämät arvot
    tietokannan vaatimia arvoja. */
    foreach ($userInputs as $userkey => $uservalue) {

        //Debugging
        //echo "Userkey: " . $userkey . "<br>";
        foreach ($dbAcceptedInputs as $dbkey => $dbvalue) {

            //Debugging
            //echo "dbKEY: " . $dbkey . "<br>";
            if($userkey == $dbkey)
            {
                //Debugging
                //echo "MATCH!: " . $userkey . " = " . $dbkey . "<br>";
                
                /* Tutkitaan array_search-metodilla löytyykö $uservalue $dbvalue-arraysta. 
                *  Array_search palauttaa arvon booleanina ja tutkitaan vastaako arvo
                *  arvoa false. Mikäli näin on muutetaan arvo NULL:iksi.
                 */
                if(array_search($uservalue, $dbvalue) === false)
                {
                    /* //Debugging

                    echo "array_search Type: " . gettype(array_search($uservalue, $dbvalue)) . ",array_search Value: " . array_search($uservalue, $dbvalue) . "<br>";
                    echo "Not right format! Nulling userinput with key: " . $userkey . ", Value: " . $uservalue . "<br>";
                    echo "Should be: ";
                    */for($i = 0; $i < count($dbvalue); $i++)
                    {
                        echo $dbvalue[$i] . "(" . strlen($dbvalue[$i]) . ")" . "(" . gettype($dbvalue[$i]) . "), ";
                    }
                    echo "<br>But you have: " . $uservalue  . "(" . strlen($uservalue) . ")" . "(" . gettype($uservalue) . ")<br><br>";
                    /**/
                    $userInputs[$userkey] = NULL;
                }
                /*  //Debugging
                else
                {
                    echo "Type: " . gettype(array_search($uservalue, $dbvalue)) . ",Value: " . array_search($uservalue, $dbvalue);
                    echo "Everything ok! <br><br>";
                }
                */
                break;
            }
        }
    }

    // foreach ($dbAcceptedInputs as $key => $value)
    // {
    //     if($key == "catch_fish_weight_method")
    //     {
    //         $userInputs[$key] = /*'Estimated';//*/$fishWeightMethod;
    //     }
    //     elseif ($key == "catch_water_salinity") 
    //     {
    //         $userInputs[$key] = /*"Brackish";//*/$waterSalinity;
    //     }
    //     elseif ($key == "catch_water_murkness") 
    //     {
    //         $userInputs[$key] = /*"Murky";//*/$waterMurkness;
    //     }
    //     elseif ($key == "catch_water_type") 
    //     {
    //         $userInputs[$key] = /*"Lake";//*/$WaterType;
    //     }
    //     elseif ($key == "catch_depth") 
    //     {
    //         $userInputs[$key] = /*"Surface";//*/$catchDepth;
    //     }
    //     elseif ($key == "catch_method") 
    //     {
    //         $userInputs[$key] = /*"Throwing";//*/$catchMethod;
    //     }
    //     elseif ($key == "catch_weather_conditions") 
    //     {
    //         $userInputs[$key] = /*"Sunny";//*/$weatherConditions;
    //     }
    //     elseif ($key == "catch_weather_visibility") 
    //     {
    //         $userInputs[$key] = /*"Darkness";//*/$weatherVisibility;
    //     }
    // }


    

    //echo array_search("Salmon", $fishTypeIdArray);
	//Koska applikaatiosta indexi alkaa 0:sta ja tietokannoissa 1:stä niin lisätään applikaatiosta saatuun indeksiin +1
	$userInputs["user_id"] +=1;

    /**Query jolla noudetaan jokaisen kalatyypin lukumäärä
     * saalistiedoista
     */

    $queryString = "SELECT fish_type, count(fish_type) AS count FROM catch_a_fish";
    $firstCondition = true;
    foreach ($userInputs as $key => $value) {
        //echo "Key: " . $key . ", Value: " . $value . "<br>";
        if($firstCondition == true && $value != NULL)
        {
            $queryString  .= " WHERE " . $key . "='" . $value . "'";

            $firstCondition = false;
        }
        else if($firstCondition == false && $value != NULL)
        {
            $queryString .= " AND " . $key . "='" . $value . "'";
        }
    }
    $queryString .= " group by fish_type";

    //echo "<br><br> QueryString: " . $queryString . "<br>";


    //Suoritetaan luotu kysely
    $sql = $dbConnection->prepare($queryString);
    $sql->execute();


    /*Luodaan kala olio jokaisesta rivistä mitä tietokannasta saadaan. Tietokantaan ollaan
    tallennettu kala foreign id:llä, joten tässä hyödynnetään käyttämällä kyseistä id:tä,
    joka vastaa fishTypeIdArray:ssa vastaavaa nimeä. Luetaan kyseisen kalalajin lukumäärä
    myös olioon.*/
    $allFishesArray = array();
    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        //print_r($row);
        $fishObject = new stdClass();
        $fishObject->fishType = $fishTypeIdArray[$row['fish_type']];
        $fishObject->count =$row['count'];
        array_push($allFishesArray, $fishObject);
        //echo "<br><br>";
    }

    /*Luodaan uusi olio, jolle asetetaan fish-atribuutiksi aikaisemmin luotu 
    $fishObject array. Tämän jälkeen muutetaan olio JSON muotoon ja echotetaan
    käyttäjälle.*/
    $mainObject = new stdClass();
    $mainObject->fish = $allFishesArray;
    $mainObjectJSON = json_encode($mainObject);
    echo $mainObjectJSON;
    

    //*************POISTOON*************

    /*$sql = $dbConnection->prepare("SELECT fish_id FROM fish WHERE fish_name= ?");
    $sql->execute(array($fishType));
    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        //echo "ASD";
        echo $row['fish_id'];
        $fishType= $row['fish_id'];
    }*/

    

    /*$sql = $dbConnection->prepare($queryString);
    $sql->execute(array($fishType));*/


    //*************POISTOON*************

    function getAcceptedInputs($_dbConnection)
    {
        //Valmistellaan query ja suoritetaan se
        $_queryString = "DESC catch_a_fish";

        $_sql = $_dbConnection->prepare($_queryString);
        $_sql->execute();

        //Luodaan array, johon tullaan tallentamaan tietokannassa olevat hyväksyttävät inputit.
        $_dbAcceptedArrays = array();

        //Noudetaan tietoja rivi riviltä kunnes palautuu null
        while($row = $_sql->fetch(PDO::FETCH_ASSOC))
        {
            /* //Debugging
            echo "{$row['Field']} - {$row['Type']}\n" . "<br>";
            */


            /*Mikäli kyseinen Type sisältää datatyypin enum tallennetaan se
            $dbAcceptedArrays:iin*/

            /**
             * Tarkastellaan preg_match-funktiolla $row['Type'] arvoa.
             * preg_match("etsittävä_merkkijono", "mistä_etsitään")
             * ^------ Tässä muodossaan palauttaa arvon true tai false
             * 
             * Seulontaan käytetään regex:in määrittelemää merkkijonoa.
             * Käytettyn Regex (Regular expression)-syntaksin selitys:
             * "/ ja /" Aloitus ja lopetus merkki.Kyseisten merkkien sisään 
             *          kirjoitetaan Regex-lauseke.
             *    ^     merkkijonon aloituskohta. Eli tässä tapauksessa aloitetaan 
             *          lukeminen kohdalta kun enum esiintyy ensimmäisen kerran
             *          $row['Type'] palauttamassa stringissä.
             *    \     jälkeen tulevaa merkkiä käsitellään juuri sellaisena kuin
             *          se on Regex lauseessa kirjoitettu.
             *    .     Tarkoittaa kaikkia merkkejä
             *    *     Tarkoittaa 0 <= merkkejä
             *    $     merkkijonon lopetuskohta. Eli tarkasteltava merkkijono päättyy
             *          merkkiin ) tässä tapauksessa.
             * 
             * Suomeksi: Etsitään $row['Type']:stä seuraavanlaisen parametrin 
             * täyttävää merkkijonoa:
             * enum('mitäTahansaTekstiäJaMinkäTahansaPituista')
             * 
             * HOX!! Välissä voi olla myös mitä tahansa merkkejä, eli voi myös olla muotoa:
             * enum('item_1', 'item_2', ... , 'item_n')
             *      
             * Lisää aiheesta:
             * https://www.php.net/manual/en/function.preg-match.php
             * https://www.guru99.com/php-regular-expressions.html
             */

            if(preg_match("/^enum\(\'(.*)\'\)$/", $row['Type']))
            {
                /* //Debugging
                echo "True:" . $row['Field'] . "<br><br>";
                */

                preg_match("/^enum\(\'(.*)\'\)$/", $row['Type'], $matches);
                /**Tässä muodossa palauttaa $matches-arrayn. Arrayn eka elementti sisältää
                 * koko regex-lausekkeen määritteen.
                 * Toinen elementti sisältää (.*) määritteen mukaisesti kaikki elementit
                 * pilkuilla eroteltuina.
                 * */

                /* //Debugging
                print_r($matches);
                */

                //Hajoitetaan pilkulla erotellut elementit itseisiksi arrayn elementeiksi
                $items = explode("','", $matches[1]);

                //Luodaan dbAcceptedArrays:iin $row['Field'] keylle array, johon luetaan
                //tiedot $items arraysta.
                $_dbAcceptedArrays[$row['Field']] = array();
                for ($i=0; $i < count($items); $i++) 
                { 
                    array_push($_dbAcceptedArrays[$row['Field']], $items[$i]);
                }
            }
        }
        foreach ($_dbAcceptedArrays as $key => $value) {
            echo "<br> FUNCTIOSSA!<br><br>";
            echo "Key: " . $key . ", Value: " . $value . "<br>";
        }
        return $_dbAcceptedArrays;
    }

    /*

    *************POISTOON*************

    function getFishtypeIdFor($_fishType, $_dbConnection)
    {
        $_sql = $_dbConnection->prepare("SELECT fish_id FROM fish WHERE fish_name= ?");
        $_sql->execute(array($_fishType));
        $_fishTypeId ="";
        while($row = $_sql->fetch(PDO::FETCH_ASSOC))
        {
            $_fishTypeId= $row['fish_id'];
        }
        echo "ASD <br>";
        echo "fishTypeId: " . $_fishTypeId . "<br>";
        return $_fishTypeId;
    }

    class Searcher
    {
        private $userInputs = array();
        private $userId = null;
        private $fishTypeId = null;
        private $lureType = null;
        private $waterType = null;
        private $fishingStyle = null;

        function setUserId($userId)
        {
            $this->userInputs['userId'] = $userId;
        }

        function getUserId()
        {
            return $this->userInputs['userId'];
        }

        function setFishTypeId($ftId)
        {
            $this->fishTypeId = $ftId;
        }

        function getFishTypeId()
        {
            return $this->fishTypeId;
        }

        function setLureType($lt)
        {
            $this->lureType = $lt;
        }

        function getLureType()
        {
            return $this->lureType;
        }

        function setWaterType($wt)
        {
            $this->waterType = $wt;
        }

        function getWaterType()
        {
            return $this->waterType;
        }

        function setFishingStyle($fs)
        {
            $this->fishingStyle = $fs;
        }

        function getFishingStyle()
        {
            return $this->fishingStyle;
        }

        
    }

        *************POISTOON*************

    */
    

?>